package io.gitlab.sklavedaniel.gaia.shared

import org.parboiled2._
import shapeless._
import BaseParser._
import ScriptParser._

import scala.util.{Failure, Try}

class ScriptParser(val input: ParserInput) extends BaseParser {

  def ScriptRule = rule {
    WSRule ~ zeroOrMore((StatementRule | MustCommandRule | MustnotCommandRule |  SetCommandRule | UnsetCommandRule |
      NumactionsfromCommandRule | RepeatsetCommandRule | RepeataddCommandRule | RepeatdelCommandRule) ~ WSRule) ~ EOI
  }

  def StatementRule: Rule1[Statement] = {
    rule {
      IdRule ~ (GlobalCommandRule | ExtendedGlobalCommandRule) ~> DefinitionValue
    }
  }

  def GlobalCommandRule = rule {
    (PageCommandRule | GotoCommandRule) ~ push(Nil)
  }

  def ExtendedGlobalCommandRule = rule {
    '{' ~ (PageCommandRule | GotoCommandRule) ~ push(Nil) ~
      oneOrMore(';' ~ (MustCommandRule | MustnotCommandRule | SetCommandRule | UnsetCommandRule | SoundCommandRule |
        RepeatsetCommandRule | RepeataddCommandRule | RepeatdelCommandRule) ~>
        ((list: List[ExtraCommandValue], value: ExtraCommandValue) => value :: list))
  }

  def ActionValueRule: Rule1[ActionCommandValue] = {
    rule {
      HorizActionCommandRule | VertActionCommandRule | MultActionCommandRule | PicCommandRule | TextCommandRule |
        UnsetCommandRule | SetCommandRule | MustCommandRule | MustnotCommandRule | SoundCommandRule | YNCommandRule |
        DelayCommandRule | GoCommandRule | ButtonsCommandRule
    }
  }

  def PageCommandRule: Rule1[PageCommandValue] = {
    import io.gitlab.sklavedaniel.gaia.shared.BiMapped.{hlistBiMapped1, hnilBiMapped}

    def params = new SingleValue(ParameterRule("text", HTMLRule)) ::
      new SingleValue(ParameterRule("media", ActionValueRule)) ::
      new SingleValue(ParameterRule("hidden", ActionValueRule)) ::
      new SingleValue(ParameterRule("instruc", ActionValueRule)) ::
      new SingleValue(ParameterRule("action", ActionValueRule)) :: HNil

    def command = CommandRule("page", params)

    rule {
      command ~> PageCommandValue
    }
  }


  def HorizActionCommandRule: Rule1[HorizActionCommandValue] = {
    def command = CommandRule("horiz", (new MultiValue(MultiParameterRule("e", ActionValueRule))) :: HNil)

    rule {
      command ~> HorizActionCommandValue
    }
  }

  def VertActionCommandRule: Rule1[VertActionCommandValue] = {
    def command = CommandRule("vert", (new MultiValue(MultiParameterRule("e", ActionValueRule))) :: HNil)

    rule {
      command ~> VertActionCommandValue
    }
  }


  def MultActionCommandRule: Rule1[MultActionCommandValue] = {
    def command = CommandRule("mult", (new MultiValue(MultiParameterRule("e", ActionValueRule))) :: HNil)

    rule {
      command ~> MultActionCommandValue
    }
  }


  def GotoCommandRule: Rule1[GotoCommandValue] = {
    def command = CommandRule("goto", new SingleValue(ParameterRule[Target]("target", TargetOrEndRule)) :: HNil)

    rule {
      command ~> GotoCommandValue
    }
  }

  def PicCommandRule: Rule1[PicCommandValue] = {
    def command = CommandRule("pic", new SingleValue(ParameterRule[String]("id", StringLiteralRule)) :: HNil)

    rule {
      command ~> PicCommandValue
    }
  }

  def TextCommandRule: Rule1[TextCommandValue] = {
    def command = CommandRule("text", new SingleValue(ParameterRule[List[MHTMLNode]]("text", HTMLRule)) :: HNil)

    rule {
      command ~> TextCommandValue
    }
  }

  def SoundCommandRule: Rule1[SoundCommandValue] = {
    def command = CommandRule("sound", new SingleValue(ParameterRule[String]("id", StringLiteralRule)) :: new SingleValue(ParameterRule[Int]("loops", NumberRule)) :: HNil)

    rule {
      command ~> SoundCommandValue
    }
  }

  def GoCommandRule: Rule1[GoCommandValue] = {
    def command = CommandRule("go", (new SingleValue(ParameterRule[Target]("target", TargetOrEndRule))) :: HNil)

    rule {
      command ~> GoCommandValue
    }
  }

  def YNCommandRule: Rule1[YNCommandValue] = {
    def command = CommandRule("yn", (new SingleValue(ParameterRule[Target]("yes", TargetOrEndRule))) :: (new SingleValue(ParameterRule[Target]("no", TargetOrEndRule))) :: HNil)

    rule {
      command ~> YNCommandValue
    }
  }


  def ButtonsCommandRule: Rule1[ButtonsCommandValue] = {
    def command = CommandRule("buttons", (new MultiValue(MultiParameterRule[List[MHTMLNode]]("cap", HTMLRule))) :: (new MultiValue(MultiParameterRule[Target]("target", TargetOrEndRule))) :: HNil)

    rule {
      command ~> ButtonsCommandValue
    }
  }

  def SetCommandRule: Rule1[SetCommandValue] = {
    def command = CommandRule("set", (new MultiValue(MultiParameterRule[Target]("action", TargetRule))) :: HNil)

    rule {
      command ~> SetCommandValue
    }
  }

  def UnsetCommandRule: Rule1[UnsetCommandValue] = {
    def command = CommandRule("unset", (new MultiValue(MultiParameterRule[Target]("action", TargetRule))) :: HNil)

    rule {
      command ~> UnsetCommandValue
    }
  }

  def MustCommandRule: Rule1[MustCommandValue] = {
    def command = CommandRule("must", (new SingleValue(ParameterRule[String]("self", IdRule))) :: (new MultiValue(MultiParameterRule[Target]("action", TargetRule))) :: HNil)

    rule {
      command ~> MustCommandValue
    }
  }

  def MustnotCommandRule: Rule1[MustnotCommandValue] = {
    def command = CommandRule("mustnot", (new SingleValue(ParameterRule[String]("self", IdRule))) :: (new MultiValue(MultiParameterRule[Target]("action", TargetRule))) :: HNil)

    rule {
      command ~> MustnotCommandValue
    }
  }

  def NumactionsfromCommandRule: Rule1[NumactionsfromCommandValue] = {
    def command = CommandRule("numactionsfrom", (new SingleValue(ParameterRule[String]("self", IdRule))) ::
      (new SingleValue(ParameterRule[String]("action", IdRule))) :: (new SingleValue(ParameterRule[Int]("count", IntegerRule))) :: HNil)

    rule {
      command ~> NumactionsfromCommandValue
    }
  }

  def RepeatCommandRule(name: String) = {
    def command = CommandRule("repeat" + name, (new SingleValue(ParameterRule[String]("target", IdRule))) ::
      (new SingleValue(ParameterRule[Int]("count", IntegerRule))) :: HNil)

    rule {
      command
    }
  }

  def RepeatsetCommandRule: Rule1[RepeatsetCommandValue] = rule {
    RepeatCommandRule("set") ~> RepeatsetCommandValue
  }

  def RepeataddCommandRule: Rule1[RepeataddCommandValue] = rule {
    RepeatCommandRule("add") ~> RepeataddCommandValue
  }

  def RepeatdelCommandRule: Rule1[RepeatdelCommandValue] = rule {
    RepeatCommandRule("del") ~> RepeatdelCommandValue
  }
}

object ScriptParser {

  sealed trait Statement

  sealed trait GlobalCommandValue

  sealed trait ExtraCommandValue extends ActionCommandValue

  case class MultActionCommandValue(value: Command[List[(Int, ActionCommandValue)] :: HNil]) extends ActionCommandValue

  case class VertActionCommandValue(value: Command[List[(Int, ActionCommandValue)] :: HNil]) extends ActionCommandValue

  case class HorizActionCommandValue(value: Command[List[(Int, ActionCommandValue)] :: HNil]) extends ActionCommandValue

  case class GotoCommandValue(value: Command[Option[Target] :: HNil]) extends GlobalCommandValue

  case class PageCommandValue(value: Command[Option[List[MHTMLNode]] :: Option[ActionCommandValue] :: Option[ActionCommandValue] :: Option[ActionCommandValue] :: Option[ActionCommandValue] :: HNil]) extends GlobalCommandValue

  case class PicCommandValue(value: Command[Option[String] :: HNil]) extends ActionCommandValue

  case class TextCommandValue(value: Command[Option[List[MHTMLNode]] :: HNil]) extends ActionCommandValue

  case class SoundCommandValue(value: Command[Option[String] :: Option[Int] :: HNil]) extends ActionCommandValue with ExtraCommandValue

  case class GoCommandValue(value: Command[Option[Target] :: HNil]) extends ActionCommandValue

  case class YNCommandValue(value: Command[Option[Target] :: Option[Target] :: HNil]) extends ActionCommandValue

  case class ButtonsCommandValue(value: Command[List[(Int, List[MHTMLNode])] :: List[(Int, Target)] :: HNil]) extends ActionCommandValue

  case class SetCommandValue(value: Command[List[(Int, Target)] :: HNil]) extends ActionCommandValue with ExtraCommandValue with Statement

  case class UnsetCommandValue(value: Command[List[(Int, Target)] :: HNil]) extends ActionCommandValue with ExtraCommandValue with Statement

  case class MustCommandValue(value: Command[Option[String] :: List[(Int, Target)] :: HNil]) extends ActionCommandValue with Statement with ExtraCommandValue

  case class MustnotCommandValue(value: Command[Option[String] :: List[(Int, Target)] :: HNil]) extends ActionCommandValue with Statement with ExtraCommandValue

  case class NumactionsfromCommandValue(value: Command[Option[String] :: Option[String] :: Option[Int] :: HNil]) extends ActionCommandValue with Statement

  case class DefinitionValue(name: String, definition: GlobalCommandValue, extra: List[ExtraCommandValue]) extends Statement

  case class RepeatsetCommandValue(value: Command[Option[String] :: Option[Int] :: HNil]) extends ActionCommandValue with Statement with ExtraCommandValue

  case class RepeataddCommandValue(value: Command[Option[String] :: Option[Int] :: HNil]) extends ActionCommandValue with Statement with ExtraCommandValue

  case class RepeatdelCommandValue(value: Command[Option[String] :: Option[Int] :: HNil]) extends ActionCommandValue with Statement with ExtraCommandValue

  def parse(s: String): Try[Seq[Statement]] = {
    val p = new ScriptParser(s)
    p.ScriptRule.run() match {
      case Failure(e: ParseError) =>
        val ne = new RuntimeException(p.formatError(e), e)
        ne.fillInStackTrace()
        Failure(ne)
      case r => r
    }
  }
}
