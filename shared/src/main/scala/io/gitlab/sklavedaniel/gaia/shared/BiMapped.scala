package io.gitlab.sklavedaniel.gaia.shared

import language.higherKinds

import shapeless.{::, HList, HNil}

trait Recursion[I[_], F1[_], F2[_], G1[_], G2[_]] {
  def rec1[A, M <: HList](x: F1[A], xs: M)(implicit ev: BiMapped[M, F1, F2, G1, G2]): I[G1[A] :: ev.Out]

  def rec2[A, M <: HList](x: F2[A], xs: M)(implicit ev: BiMapped[M, F1, F2, G1, G2]): I[G2[A] :: ev.Out]

  def nil(): I[HNil]
}

trait Recursion2[F1[_], F2[_], G1[_], G2[_]] {
  def rec1[A, M <: HList](x1: G1[A], x2: G1[A], xs1: M, xs2: M)(implicit ev: BiMapped.Aux[_, F1, F2, G1, G2, M]): G1[A] :: M

  def rec2[A, M <: HList](x1: G2[A], x2: G2[A], xs1: M, xs2: M)(implicit ev: BiMapped.Aux[_, F1, F2, G1, G2, M]): G2[A] :: M
}

trait CConst[G1[_], G2[_]] {
  def const1[A](): G1[A]

  def const2[A](): G2[A]
}

trait BiMapped[L <: HList, F1[_], F2[_], G1[_], G2[_]] {
  type Out <: HList

  def const(empty: CConst[G1, G2]): Out

  def recurse[I[_]](list: L)(cons: Recursion[I, F1, F2, G1, G2]): I[Out]

  def recurse2(list1: Out, list2: Out)(cons: Recursion2[F1, F2, G1, G2]): Out
}

object BiMapped {
  type Aux[L <: HList, F1[_], F2[_], G1[_], G2[_], Out0 <: HList] = BiMapped[L, F1, F2, G1, G2] {type Out = Out0}

  implicit def hnilBiMapped[F1[_], F2[_], G1[_], G2[_]]: Aux[HNil, F1, F2, G1, G2, HNil] = new BiMapped[HNil, F1, F2, G1, G2] {
    type Out = HNil

    override def recurse[I[_]](list: HNil)(rec: Recursion[I, F1, F2, G1, G2]): I[HNil] = rec.nil()

    override def recurse2(list1: HNil, list2: HNil)(rec: Recursion2[F1, F2, G1, G2]): HNil = HNil

    override def const(c: CConst[G1, G2]): HNil = HNil
  }

  implicit def hlistBiMapped1[H, T <: HList, F1[_], F2[_], G1[_], G2[_], TCM <: HList](
    implicit mt: BiMapped.Aux[T, F1, F2, G1, G2, TCM]
  ): Aux[F1[H] :: T, F1, F2, G1, G2, G1[H] :: TCM] =
    new BiMapped[F1[H] :: T, F1, F2, G1, G2] {
      type Out = G1[H] :: TCM

      override def recurse[I[_]](list: F1[H] :: T)(rec: Recursion[I, F1, F2, G1, G2]): I[G1[H] :: TCM] = rec.rec1(list.head, list.tail)

      override def recurse2(list1: G1[H] :: TCM, list2: G1[H] :: TCM)(rec: Recursion2[F1, F2, G1, G2]): G1[H] :: TCM = rec.rec1(list1.head, list2.head, list1.tail, list2.tail)

      override def const(c: CConst[G1, G2]): ::[G1[H], TCM] = c.const1[H]() :: mt.const(c)
    }

  implicit def hlistBiMapped2[H, T <: HList, F1[_], F2[_], G1[_], G2[_], TCM <: HList](
    implicit mt: BiMapped.Aux[T, F1, F2, G1, G2, TCM]
  ): Aux[F2[H] :: T, F1, F2, G1, G2, G2[H] :: TCM] =
    new BiMapped[F2[H] :: T, F1, F2, G1, G2] {
      type Out = G2[H] :: TCM

      override def recurse[I[_]](list: F2[H] :: T)(rec: Recursion[I, F1, F2, G1, G2]): I[G2[H] :: TCM] = rec.rec2(list.head, list.tail)

      override def recurse2(list1: G2[H] :: TCM, list2: G2[H] :: TCM)(rec: Recursion2[F1, F2, G1, G2]): G2[H] :: TCM = rec.rec2(list1.head, list2.head, list1.tail, list2.tail)

      override def const(c: CConst[G1, G2]): ::[G2[H], TCM] = c.const2[H]() :: mt.const(c)
    }
}
