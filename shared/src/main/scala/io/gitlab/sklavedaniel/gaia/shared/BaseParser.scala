package io.gitlab.sklavedaniel.gaia.shared

import io.gitlab.sklavedaniel.gaia.shared.BaseParser._
import org.parboiled2._
import shapeless._

abstract class BaseParser extends Parser {

  abstract class ValueType[T](rule: => Rule1[Option[T]]) {
    def apply(): Rule1[Option[T]] = rule
  }

  class SingleValue[T](rule: => Rule1[Option[T]]) extends ValueType[T](rule)

  class MultiValue[T](rule: => Rule1[Option[T]]) extends ValueType[T](rule)

  def zeroOrMoreOf[L <: HList](list: => L)(implicit ev: BiMapped[L, SingleValue, MultiValue, Option, List]): Rule1[ev.Out] = rule {
    push(ev.const(new CConst[Option, List] {
      override def const1[A](): Option[A] = None

      override def const2[A](): List[A] = Nil
    }) :: HNil) ~ zeroOrMore(typedOneOf(list) ~> ((m: ev.Out, x: ev.Out) => ((merge(m, x)(ev)): ev.Out) :: HNil)).separatedBy(',')
  }

  def typedOneOf[L <: HList](list: => L)(implicit ev: BiMapped[L, SingleValue, MultiValue, Option, List]): Rule1[ev.Out] = {
    def helper[L2 <: HList](list: => L2)(implicit ev: BiMapped[L2, SingleValue, MultiValue, Option, List]): Rule1[ev.Out] = ev.recurse[Rule1](list)(new Recursion[Rule1, SingleValue, MultiValue, Option, List] {
      override def rec1[A, M <: HList](x: SingleValue[A], xs: M)(implicit ev2: BiMapped[M, SingleValue, MultiValue, Option, List]): Rule1[Option[A] :: ev2.Out] = rule {
        ev2.recurse(xs)(this) ~> ((rs: ev2.Out) => (None :: rs) :: HNil)
      }

      override def rec2[A, M <: HList](x: MultiValue[A], xs: M)(implicit ev2: BiMapped[M, SingleValue, MultiValue, Option, List]): Rule1[List[A] :: ev2.Out] = rule {
        ev2.recurse(xs)(this) ~> ((rs: ev2.Out) => (Nil :: rs) :: HNil)
      }

      def nil() = rule {
        push(HNil :: HNil)
      }
    })

    ev.recurse[Rule1](list)(new Recursion[Rule1, SingleValue, MultiValue, Option, List] {
      override def rec1[A, M <: HList](x: SingleValue[A], xs: M)(implicit ev2: BiMapped[M, SingleValue, MultiValue, Option, List]): Rule1[Option[A] :: ev2.Out] = rule {
        (x() ~ helper(xs) ~> ((r: Option[A], rs: ev2.Out) => (r :: rs) :: HNil)) | (ev2.recurse(xs)(this) ~> ((rs: ev2.Out) => {
          (None :: rs) :: HNil
        }))
      }

      override def rec2[A, M <: HList](x: MultiValue[A], xs: M)(implicit ev2: BiMapped[M, SingleValue, MultiValue, Option, List]): Rule1[List[A] :: ev2.Out] = rule {
        (x() ~ helper(xs) ~> ((r: Option[A], rs: ev2.Out) => (r.toList :: rs) :: HNil)) | (ev2.recurse(xs)(this) ~> ((rs: ev2.Out) => {
          (Nil :: rs) :: HNil
        }))
      }

      def nil() = rule {
        push(HNil :: HNil)
      }
    })
  }

  def merge[L <: HList](l1: L, l2: L)(implicit ev: BiMapped.Aux[_, SingleValue, MultiValue, Option, List, L]): L = {
    ev.recurse2(l1, l2)(new Recursion2[SingleValue, MultiValue, Option, List] {
      override def rec1[A, M <: HList](x1: Option[A], x2: Option[A], xs1: M, xs2: M)(implicit ev2: BiMapped.Aux[_, SingleValue, MultiValue, Option, List, M]): Option[A] :: M = {
        x1.orElse(x2) :: ev2.recurse2(xs1, xs2)(this)
      }

      override def rec2[A, M <: HList](x1: List[A], x2: List[A], xs1: M, xs2: M)(implicit ev2: BiMapped.Aux[_, SingleValue, MultiValue, Option, List, M]): List[A] :: M = {
        (x1 ++ x2) :: ev2.recurse2(xs1, xs2)(this)
      }

    })
  }


  def CommandRule[L <: HList](name: String, parameter: => L)(implicit ev: BiMapped[L, SingleValue, MultiValue, Option, List]): Rule1[Command[ev.Out]] = rule {
    capture(name) ~ '(' ~ WSRule ~ zeroOrMoreOf(parameter) ~ ')' ~> ((p: String, r: ev.Out) => Command(p, r))
  }

  def ParameterRule[A](name: String, value: => Rule1[A]): Rule1[Option[A]] = rule {
    name ~ ':' ~ WSRule ~ optional(value ~ WSRule)
  }

  def MultiParameterRule[A](name: String, value: => Rule1[A]): Rule1[Option[(Int, A)]] = rule {
    name ~ NumberRule ~ ':' ~ WSRule ~ optional(value ~ WSRule) ~> ((x: Int, y: Option[A]) => y.map(tmp => (x, tmp)))
  }

  def TargetRule: Rule1[Target] = rule {
    (IdRule ~> ((s: String) => SingleTarget(s))) | RangeCommandRule
  }

  def TargetOrEndRule: Rule1[Target] = rule {
    ('#' ~ push(EndTarget)) | TargetRule
  }

  def RangeCommandRule: Rule1[RangeTarget] = {
    val cmd = CommandRule("range",
      (new SingleValue(ParameterRule[Int]("from", NumberRule))) :: (new SingleValue(ParameterRule[Int]("to", NumberRule)))
        :: (new SingleValue(ParameterRule[String]("prefix", StringLiteralRule))) :: HNil)

    rule {
      cmd ~> {
        (c: Command[Option[Int] :: Option[Int] :: Option[String] :: HNil]) =>
          RangeTarget(c.parameter.tail.tail.head.getOrElse(""), c.parameter.head.get, c.parameter.tail.head.get)
      }
    }
  }

  def DelayCommandRule: Rule1[DelayCommandValue] = {
    def timeRules = rule {
      RandomCommandRule | (TimeRule ~> {
        (x: Double) => RandomNumber(x, x)
      })
    }

    def params = new SingleValue(ParameterRule("target", TargetOrEndRule)) ::
      new SingleValue(ParameterRule[String]("style", StringLiteralRule)) ::
      new SingleValue(ParameterRule[RandomNumber]("time", timeRules)) :: HNil

    rule {
      CommandRule("delay", params) ~> DelayCommandValue
    }
  }

  def RandomCommandRule: Rule1[RandomNumber] = {
    def cmd = CommandRule("random",
      (new SingleValue(ParameterRule[Int]("min", NumberRule))) :: (new SingleValue(ParameterRule[Int]("max", NumberRule))) :: HNil)

    rule {
      cmd ~> {
        (c: Command[Option[Int] :: Option[Int] :: HNil]) =>
          RandomNumber(c.parameter.head.get, c.parameter.tail.head.get)
      }
    }
  }

  val symbolStartChars = CharPredicate.from(Character.isJavaIdentifierStart _)
  val symbolChars = CharPredicate.from(Character.isJavaIdentifierPart _) ++ "-."

  def IdRule = rule {
    capture(zeroOrMore(symbolChars)) ~ '#'
  }

  def SymbolRule = rule {
    capture(symbolStartChars ~ zeroOrMore(symbolChars))
  }

  def NumberRule = rule {
    capture(oneOrMore(CharPredicate.Digit)) ~> (x => x.toInt)
  }

  def IntegerRule = rule {
    capture(optional('-') ~ oneOrMore(CharPredicate.Digit)) ~> (x => x.toInt)
  }


  def FloatingPointRule = rule {
    capture(oneOrMore(CharPredicate.Digit) ~ optional('.' ~ oneOrMore(CharPredicate.Digit))) ~> (x => x.toDouble)
  }

  def TimeRule = rule {
    optional(FloatingPointRule ~ "hrs") ~ optional(FloatingPointRule ~ "min") ~ optional(FloatingPointRule ~ "sec") ~>
      ((h: Option[Double], m: Option[Double], s: Option[Double]) => 60 * (60 * h.getOrElse(0.0) + m.getOrElse(0.0)) + s.getOrElse(0.0))
  }

  def StringLiteralRule: Rule1[String] = rule {
    SymbolRule | StringLiteralMetaRule('\'', ",)") | StringLiteralMetaRule('"', ",)")
  }

  def StringLiteralMetaRule(separator: Char, end: String): Rule1[String] = {
    val noSeparator = CharPredicate.All -- separator
    rule {
      separator ~ capture(zeroOrMore(noSeparator) ~
        zeroOrMore(oneOrMore(separator) ~ WSRule ~ noneOf(end) ~ zeroOrMore(noSeparator))) ~ separator
    }
  }

  def HTMLRule: Rule1[List[MHTMLNode]] = rule {
    ('"' ~ HTMLMixedRule('"') ~ '"') | ('\'' ~ HTMLMixedRule('\'') ~ '\'')
  }

  def HTMLTagRule(separator: Char): Rule1[MHTMLTag] = rule {
    '<' ~ SymbolRule ~ WSRule ~ HTMLAttrsRule ~ WSRule ~ '>' ~ HTMLMixedRule(separator) ~ "</" ~ SymbolRule ~ '>' ~> {
      (name: String, attrs: List[(String, List[MHTMLTextNode])], content: List[MHTMLNode], name2: String) =>
        (test(name == name2) | fail(s"Start tag was named $name but end tag was $name2.")) ~ push(MHTMLTag(name, attrs, content))
    }
  }


  def HTMLEmptyTagRule: Rule1[MHTMLTag] = rule {
    '<' ~ SymbolRule ~ WSRule ~ HTMLAttrsRule ~ WSRule ~ "/>" ~> {
      (name: String, attrs: List[(String, List[MHTMLTextNode])]) => MHTMLTag(name, attrs, Nil)
    }
  }

  def HTMLAttrsRule: Rule1[List[(String, List[MHTMLTextNode])]] = rule {
    push(Nil) ~ zeroOrMore(SymbolRule ~ WSRule ~ '=' ~ WSRule ~ (HTMLAttrValueRule('"') | HTMLAttrValueRule('\'')) ~> {
      (list: List[(String, List[MHTMLTextNode])], name, value) => (name, value) :: list
    }).separatedBy(WSRule)
  }

  def HTMLMixedRule(separator: Char): Rule1[List[MHTMLNode]] = rule {
    (push(Nil) ~ zeroOrMore((HTMLTagRule(separator) | HTMLEmptyTagRule | HTMLRefRule | HTMLOuterTextRule(separator) | HTMLAmpRule) ~> {
      (list: List[MHTMLNode], node: MHTMLNode) => node :: list
    })) ~> ((x: List[MHTMLNode]) => x.reverse)
  }

  def HTMLAttrValueRule(separator: Char): Rule1[List[MHTMLTextNode]] = rule {
    push(Nil) ~ separator ~ zeroOrMore((HTMLStringRule(separator) | HTMLRefRule | HTMLAmpRule) ~> {
      (list: List[MHTMLTextNode], text: MHTMLTextNode) => text :: list
    }) ~ separator
  }

  def HTMLStringRule(separator: Char): Rule1[MHTMLTextNode] = {
    val noQuote = (CharPredicate.All -- '&') -- separator
    rule {
      capture(oneOrMore(noQuote)) ~> ((s: String) => MHTMLText(s))
    }
  }

  def HTMLAmpRule: Rule1[MHTMLTextNode] = rule {
    '&' ~ push(MHTMLText("&"))
  }

  def HTMLOuterTextRule(separator: Char): Rule1[MHTMLNode] = {
    val noSeparator = (CharPredicate.All -- "<&") -- separator
    val end = ",)"
    rule {
      capture(oneOrMore(noSeparator) ~
        zeroOrMore(oneOrMore(separator) ~ WSRule ~ noneOf(end) ~ zeroOrMore(noSeparator))) ~> ((s: String) => MHTMLText(s))
    }
  }

  def HTMLRefRule: Rule1[MHTMLTextNode] = rule {
    '&' ~ SymbolRule ~ ';' ~> ((s: String) => MHTMLRef(s))
  }

  def WSRule = rule {
    zeroOrMore(anyOf(" \t\r\n"))
  }
}

object BaseParser {

  import upickle.default.{ReadWriter => RW, macroRW}

  sealed trait Target

  object Target {
    implicit val readWriter: RW[Target] = RW.merge(
      macroRW[EndTarget.type], macroRW[SingleTarget], macroRW[RangeTarget]
    )
  }

  case object EndTarget extends Target

  case class SingleTarget(id: String) extends Target

  object SingleTarget {
    implicit def rw: RW[SingleTarget] = macroRW
  }

  case class RangeTarget(prefix: String, from: Int, to: Int) extends Target

  object RangeTarget {
    implicit def rw: RW[SingleTarget] = macroRW
  }

  case class RandomNumber(min: Double, max: Double)

  object RandomNumber {
    implicit def rw: RW[RandomNumber] = macroRW
  }

  case class Command[L <: HList](name: String, parameter: L)

  trait ActionCommandValue

  case class DelayCommandValue(value: Command[Option[Target] :: Option[String] :: Option[RandomNumber] :: HNil]) extends ActionCommandValue

  sealed trait MHTMLNode

  object MHTMLNode {
    implicit val readWriter: RW[MHTMLNode] = RW.merge(
      macroRW[MHTMLTextNode], macroRW[MHTMLTag]
    )
  }

  sealed trait MHTMLTextNode extends MHTMLNode

  object MHTMLTextNode {
    implicit val readWriter: RW[MHTMLTextNode] = RW.merge(
      macroRW[MHTMLText], macroRW[MHTMLRef]
    )
  }

  case class MHTMLRef(name: String) extends MHTMLTextNode

  object MHTMLRef {
    implicit def rw: RW[MHTMLRef] = macroRW
  }

  case class MHTMLText(text: String) extends MHTMLTextNode

  object MHTMLText {
    implicit def rw: RW[MHTMLText] = macroRW
  }

  case class MHTMLTag(name: String, attrs: List[(String, List[MHTMLTextNode])], content: List[MHTMLNode]) extends MHTMLNode

  object MHTMLTag {
    implicit def rw: RW[MHTMLTag] = macroRW
  }

}
