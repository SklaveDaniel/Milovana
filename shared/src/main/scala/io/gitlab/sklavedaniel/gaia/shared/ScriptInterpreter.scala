package io.gitlab.sklavedaniel.gaia.shared

import io.gitlab.sklavedaniel.gaia.shared.BaseParser._
import io.gitlab.sklavedaniel.gaia.shared.ScriptParser._
import shapeless._
import upickle.default.{macroRW, ReadWriter => RW}

import scala.collection.SortedMap
import scala.util.Random

object ScriptInterpreter {

  case class TeaseInfo(title: String, author: String, authorId: String, source: String)

  object TeaseInfo {
    implicit def rw: RW[TeaseInfo] = macroRW
  }

  case class Script(script: Map[String, ControlLocation], unreachable: Set[String], undefined: Set[String], notargets: Set[String])

  object Script {
    implicit def rw: RW[Script] = macroRW
  }

  case class Delay(delay: RandomNumber, target: Target)

  object Delay {
    implicit def rw: RW[Delay] = macroRW
  }

  case class Sound(id: String, loop: Int)

  object Sound {
    implicit def rw: RW[Sound] = macroRW
  }

  sealed trait TimerStyle

  object TimerStyle {
    implicit val readWriter: RW[TimerStyle] = RW.merge(
      macroRW[TimerStyleVisible.type], macroRW[TimerStyleSecret.type]
    )
  }

  case object TimerStyleVisible extends TimerStyle

  case object TimerStyleSecret extends TimerStyle

  sealed trait CounterAction

  object CounterAction {
    implicit val readWriter: RW[CounterAction] = RW.merge(
      macroRW[SetCounter], macroRW[ChangeCounter], macroRW[InitCounter]
    )
  }

  case class SetCounter(value: Int) extends CounterAction

  object SetCounter {
    implicit def rw: RW[SetCounter] = macroRW
  }

  case class ChangeCounter(delta: Int) extends CounterAction

  object ChangeCounter {
    implicit def rw: RW[ChangeCounter] = macroRW
  }

  case class InitCounter(from: String) extends CounterAction

  object InitCounter {
    implicit def rw: RW[InitCounter] = macroRW
  }

  sealed trait CounterCondition

  object CounterCondition {
    implicit val readWriter: RW[CounterCondition] = RW.merge(
      macroRW[LTEQCounter], macroRW[LTCounter]
    )
  }

  case class LTEQCounter(other: String, delta: Int = 0) extends CounterCondition

  object LTEQCounter {
    implicit def rw: RW[LTEQCounter] = macroRW
  }

  case class LTCounter(other: String, delta: Int = 0) extends CounterCondition

  object LTCounter {
    implicit def rw: RW[LTCounter] = macroRW
  }

  sealed trait ControlLocation {
    def actions: Seq[(String, CounterAction)]

    def conditions: Seq[(String, CounterCondition)]
  }

  object ControlLocation {
    implicit val readWriter: RW[ControlLocation] = RW.merge(
      macroRW[Page], macroRW[Goto]
    )
  }

  case class Page(mainContent: Option[PageContent], sidebarContent: Option[PageContent], sound: Option[Sound], delay: Option[Delay],
    actions: Seq[(String, CounterAction)], conditions: Seq[(String, CounterCondition)]
  ) extends ControlLocation

  object Page {
    implicit def rw: RW[Page] = macroRW
  }

  sealed trait PageContent

  object PageContent {
    implicit val readWriter: RW[PageContent] = RW.merge(
      macroRW[VerticalBox], macroRW[HorizontalBox], macroRW[Picture], macroRW[Button], macroRW[Timer], macroRW[HTMLContent]
    )
  }

  case class VerticalBox(content: Seq[PageContent]) extends PageContent

  object VerticalBox {
    implicit def rw: RW[VerticalBox] = macroRW
  }

  case class HorizontalBox(content: Seq[PageContent]) extends PageContent

  object HorizontalBox {
    implicit def rw: RW[HorizontalBox] = macroRW
  }

  case class Picture(uri: String) extends PageContent

  object Picture {
    implicit def rw: RW[Picture] = macroRW
  }

  case class Button(content: List[MHTMLNode], target: Target) extends PageContent

  object Button {
    implicit def rw: RW[Button] = macroRW
  }

  case class Timer(style: TimerStyle) extends PageContent

  object Timer {
    implicit def rw: RW[Timer] = macroRW
  }

  case class HTMLContent(content: List[MHTMLNode]) extends PageContent

  object HTMLContent {
    implicit def rw: RW[HTMLContent] = macroRW
  }

  case class Goto(target: Target, actions: Seq[(String, CounterAction)], conditions: Seq[(String, CounterCondition)]) extends ControlLocation

  object Goto {
    implicit def rw: RW[Goto] = macroRW
  }

  case class Configuration(page: Option[String], counters: Map[String, Int])

  object Configuration {
    implicit def rw: RW[Configuration] = macroRW
  }

  def resolveTarget(target: Target, script: Script): Seq[String] = resolveTarget(target, script.script.keySet)

  def resolveTarget(target: Target, script: Set[String]): Seq[String] = target match {
    case SingleTarget(id) =>
      List(id)
    case RangeTarget(prefix, from, to) =>
      Range.inclusive(from, to).map(x => prefix + x).filter(script)
    case EndTarget =>
      Nil
  }

  val initial = Configuration(Some(""), Map())

  def nextIds(action: Target, configuration: Configuration, script: Script): List[String] =
    action match {
      case EndTarget =>
        Nil
      case SingleTarget(id) =>
        List(id)
      case rt: RangeTarget =>
        resolveTarget(rt, script).filter { id =>
          script.script.get(id) match {
            case Some(page) =>
              page.conditions.forall { case (counter, condition) =>
                val value = configuration.counters.getOrElse(counter, 0)
                condition match {
                  case LTCounter(other, delta) =>
                    value < delta + configuration.counters.getOrElse(other, 0)
                  case LTEQCounter(other, delta) =>
                    value <= delta + configuration.counters.getOrElse(other, 0)
                }
              }
            case None => true
          }
        }.toList
    }

  def nextConfiguration(target: String, configuration: Configuration, script: Script) = {
    script.script.get(target) match {
      case Some(page) =>
        val newCounters = page.actions.foldLeft(configuration.counters) { case (counters, (counter, action)) =>
          val oldValue = counters.getOrElse(counter, 0)
          val newValue = action match {
            case InitCounter(other) =>
              if (oldValue == 0) {
                counters.getOrElse(other, 0)
              } else {
                oldValue
              }
            case ChangeCounter(delta) => oldValue + delta
            case SetCounter(value) => value
          }
          counters + (counter -> newValue)
        }
        Configuration(Some(target), newCounters)
      case None =>
        Configuration(None, configuration.counters)
    }
  }

  def next(action: Target, configuration: Configuration, script: Script, random: Random) = {
    val ids = nextIds(action, configuration, script)
    if (ids.isEmpty) {
      Configuration(None, configuration.counters)
    } else {
      val id = ids(random.nextInt(ids.size))
      nextConfiguration(id, configuration, script)
    }
  }

  def toScript(rules: Seq[Statement]): Script = {

    def getActions(definition: DefinitionValue): Stream[ActionCommandValue] = {
      val ret = definition.definition match {
        case p: PageCommandValue =>
          getActionsForPage(p)
        case _ => Stream.empty
      }
      ret ++ definition.extra.toStream
    }

    def getActionsForPage(page: PageCommandValue): Stream[ActionCommandValue] = {
      val Command(_, text :: media :: hidden :: instruct :: action :: HNil) = page.value
      media.toStream.flatMap(getActionsRec) ++ hidden.toStream.flatMap(getActionsRec) ++ instruct.toStream.flatMap(getActionsRec) ++
        action.toStream.flatMap(getActionsRec)
    }

    def getActionsRec(cmd: ActionCommandValue): Stream[ActionCommandValue] = {
      cmd match {
        case MultActionCommandValue(Command(_, list :: HNil)) =>
          list.toStream.flatMap(x => getActionsRec(x._2))
        case HorizActionCommandValue(Command(_, list :: HNil)) =>
          list.toStream.flatMap(x => getActionsRec(x._2))
        case VertActionCommandValue(Command(_, list :: HNil)) =>
          list.toStream.flatMap(x => getActionsRec(x._2))
        case a => Stream(a)
      }
    }

    val definitions: Map[String, DefinitionValue] = rules.collect {
      case c@DefinitionValue(name, _, _) => (name, c)
    }.toMap

    val conditions: Map[String, Seq[ActionCommandValue]] = (definitions.toStream.flatMap(x => getActions(x._2)) ++ rules).collect {
      case c@MustCommandValue(Command(_, Some(self) :: _)) => (self, c)
      case c@MustnotCommandValue(Command(_, Some(self) :: _)) => (self, c)
      case c@NumactionsfromCommandValue(Command(_, Some(self) :: _)) => (self, c)
    }.groupBy(_._1).mapValues(_.map(_._2))

    def initialActions: Stream[ActionCommandValue] = rules.toStream.collect {
      case c: SetCommandValue => c
      case c: UnsetCommandValue => c
      case c: RepeatsetCommandValue => c
      case c: RepeataddCommandValue => c
      case c: RepeatdelCommandValue => c
    }

    val mustConditions: Map[String, Seq[MustCommandValue]] = conditions.mapValues { d =>
      d.collect {
        case c: MustCommandValue => c
      }
    }
    val mustnotConditions: Map[String, Seq[MustnotCommandValue]] = conditions.mapValues { d =>
      d.collect {
        case c: MustnotCommandValue => c
      }
    }
    val numactionsfromConditions: Map[String, Seq[NumactionsfromCommandValue]] = conditions.mapValues { d =>
      d.collect {
        case c: NumactionsfromCommandValue => c
      }
    }

    val storeCounterRequired: Set[String] = numactionsfromConditions.values.flatMap(_.map {
      case NumactionsfromCommandValue(Command(_, _ :: Some(action) :: _)) => action
    }).toSet

    val counterRequired = storeCounterRequired.nonEmpty

    def getTargetsForPage(page: PageCommandValue): Stream[Target] = getActionsForPage(page).flatMap {
      case DelayCommandValue(Command(_, target :: _)) => target.toStream
      case ButtonsCommandValue(Command(_, _ :: targets :: HNil)) => targets.map(_._2).toStream
      case YNCommandValue(Command(_, yestarget :: notarget :: HNil)) => yestarget.toStream ++ notarget.toStream
      case GoCommandValue(Command(_, target :: HNil)) => target.toStream
      case _ => Stream.empty
    }

    def getTargets(definition: DefinitionValue): Stream[Target] = definition.definition match {
      case p: PageCommandValue => getTargetsForPage(p)
      case GotoCommandValue(Command(_, target :: HNil)) => target.toStream
    }

    def inrange: Stream[String] = definitions.values.toStream.flatMap(getTargets).flatMap {
      case target: RangeTarget => resolveTarget(target, definitions.keySet)
      case _ => Nil
    }

    val setRequired: Set[String] = (inrange ++ mustConditions.values.toStream.flatten.flatMap {
      case MustCommandValue(Command(_, _ :: list :: HNil)) => list.toStream.flatMap(x => resolveTarget(x._2, definitions.keySet))
      case _ => Nil
    } ++ mustnotConditions.values.toStream.flatten.flatMap {
      case MustnotCommandValue(Command(_, _ :: list :: HNil)) => list.toStream.flatMap(x => resolveTarget(x._2, definitions.keySet))
      case _ => Nil
    }).toSet

    val setCountRequired: Set[String] = (initialActions ++ definitions.values.toStream.flatMap(getActions)).collect {
      case RepeatsetCommandValue(Command(_, Some(action) :: _)) => action
      case RepeataddCommandValue(Command(_, Some(action) :: _)) => action
      case RepeatdelCommandValue(Command(_, Some(action) :: _)) => action
    }.toSet

    def getSound(definition: DefinitionValue): Stream[Sound] = getActions(definition).collect {
      case SoundCommandValue(Command(_, Some(id) :: loop :: HNil)) => Sound(id, loop.getOrElse(1))
    }

    def getDelay(definition: DefinitionValue): Stream[Delay] = getActions(definition).collect {
      case DelayCommandValue(Command(_, Some(target) :: _ :: Some(delay) :: HNil)) => Delay(delay, target)
    }

    def getCounterActions(definition: DefinitionValue): Stream[(String, CounterAction)] = {
      val explizitActions = ((if (definition.name == "start") initialActions else Stream.empty)
        ++ getActions(definition)).flatMap {
        case SetCommandValue(Command(_, list :: HNil)) =>
          list.flatMap(x => resolveTarget(x._2, definitions.keySet)).map { target =>
            if (setCountRequired(definition.name)) {
              (target, ChangeCounter(1))
            } else {
              (target, SetCounter(1))
            }
          }
        case UnsetCommandValue(Command(_, list :: HNil)) =>
          list.flatMap(x => resolveTarget(x._2, definitions.keySet)).map { target =>
            (target, SetCounter(0))
          }
        case RepeatsetCommandValue(Command(_, Some(target) :: Some(count) :: HNil)) =>
          Stream((target + "$r", SetCounter(count - 1)))
        case RepeataddCommandValue(Command(_, Some(target) :: Some(count) :: HNil)) =>
          Stream((target + "$r", ChangeCounter(count)))
        case RepeatdelCommandValue(Command(_, Some(target) :: Some(count) :: HNil)) =>
          Stream((target + "$r", ChangeCounter(count)))
        case _ => Stream()
      }

      val setCounter = if (setCountRequired(definition.name)) {
        Stream((definition.name, ChangeCounter(1)))
      } else if (setRequired(definition.name)) {
        Stream((definition.name, SetCounter(1)))
      } else {
        Stream.empty
      }
      val updateCounter = if (counterRequired) {
        Stream(("$c", ChangeCounter(1)))
      } else {
        Stream.empty
      }
      val storeCounter = if (storeCounterRequired(definition.name)) {
        Stream((definition.name + "$c", InitCounter("$c")))
      } else {
        Stream.empty
      }
      setCounter ++ updateCounter ++ storeCounter ++ explizitActions
    }

    def getCounterConditions(definition: DefinitionValue): Stream[(String, CounterCondition)] = {
      val explicitConditions = mustConditions.getOrElse(definition.name, Seq()).flatMap {
        case MustCommandValue(Command(_, _ :: list :: HNil)) => list.flatMap(x => resolveTarget(x._2, definitions.keySet)).map { target =>
          (target + "$r", LTEQCounter(target, -1))
        }
      } ++ mustnotConditions.getOrElse(definition.name, Seq()).flatMap {
        case MustnotCommandValue(Command(_, _ :: list :: HNil)) => list.flatMap(x => resolveTarget(x._2, definitions.keySet)).map { target =>
          (target, LTCounter(target + "$r", 1))
        }
      } ++ numactionsfromConditions.getOrElse(definition.name, Seq()).map {
        case NumactionsfromCommandValue(Command(_, _ :: Some(target) :: Some(count) :: HNil)) =>
          (target + "$c", LTCounter("$c", -count))
      }
      val selfMustnot = Stream((definition.name, LTCounter(definition.name + "$r", 1)))
      selfMustnot  ++ explicitConditions
    }

    def getPageContent(name: String, cmd: ActionCommandValue): List[PageContent] = {
      cmd match {
        case MultActionCommandValue(Command(_, list :: HNil)) =>
          SortedMap(list: _*).flatMap(x => getPageContent(name, x._2)).toList
        case HorizActionCommandValue(Command(_, list :: HNil)) =>
          List(HorizontalBox(SortedMap(list: _*).flatMap(x => getPageContent(name, x._2).toList).toList))
        case VertActionCommandValue(Command(_, list :: HNil)) =>
          List(VerticalBox(SortedMap(list: _*).flatMap(x => getPageContent(name, x._2)).toList))
        case DelayCommandValue(Command(_, target :: style :: delay :: HNil)) =>
          style match {
            case Some("hidden") => Nil
            case Some("secret") => List(Timer(TimerStyleSecret))
            case _ => List(Timer(TimerStyleVisible))
          }
        case ButtonsCommandValue(Command(_, captions :: targets :: HNil)) =>
          assert(captions.size == targets.size, "Different number of captions and targets on page $name.")
          val map = SortedMap(targets: _*)
          SortedMap(captions: _*).map(x => Button(x._2, map(x._1))).toList
        case YNCommandValue(Command(_, yestarget :: notarget :: HNil)) =>
          List(Button(List(MHTMLText("Yes")), yestarget.get), Button(List(MHTMLText("No")), notarget.get))
        case GoCommandValue(Command(_, target :: HNil)) =>
          List(Button(List(MHTMLText("Continue")), target.get))
        case PicCommandValue(Command(_, id :: HNil)) =>
          List(Picture(id.get))
        case TextCommandValue(Command(_, text :: HNil)) =>
          List(HTMLContent(text.get))
        case _ => Nil
      }
    }

    val states = definitions.map {
      case (name, d@DefinitionValue(_, x, _)) =>
        val actions = getCounterActions(d)
        val conditions = getCounterConditions(d)
        x match {
          case PageCommandValue(Command(_, text :: media :: hidden :: instruct :: action :: HNil)) =>
            val sound = getSound(d)
            assert(sound.size <= 1, s"More than one sound found on page $name.")
            val delay = getDelay(d)
            assert(delay.size <= 1, s"More than one delay found on page $name.")
            val mainContent = VerticalBox(media.map(x => getPageContent(name, x)).getOrElse(Nil) ++ text.map(s => List(HTMLContent(s))).getOrElse(Nil) ++ instruct.map(x => getPageContent(name, x)).getOrElse(Nil))
            val sidebarContent = VerticalBox(action.map(x => getPageContent(name, x)).getOrElse(Nil))
            (name, Page(Some(mainContent), Some(sidebarContent), sound.headOption, delay.headOption, actions, conditions))
          case GotoCommandValue(Command(_, target :: HNil)) =>
            (name, Goto(target.get, actions, conditions))
        }
    }

    val all = definitions.values.toStream.flatMap(getTargets).flatMap {
      case target: RangeTarget => resolveTarget(target, definitions.keySet)
      case SingleTarget(target) => List(target)
      case EndTarget => Nil
    }.toSet

    val ends= definitions.values.toStream.flatMap { d =>
      if (getTargets(d).exists {
        case EndTarget => true
        case _ => false
      }) Some(d.name) else None
    }.toSet

    Script(states, (definitions.keySet -- all) - "start", all -- definitions.keySet, ends)
  }

}