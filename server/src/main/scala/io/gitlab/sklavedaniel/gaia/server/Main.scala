package io.gitlab.sklavedaniel.gaia.server

import java.nio.file.{Files, Path, Paths}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.MediaType.Gzipped
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{Uri, _}
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import io.udash.logging.CrossLogging

object Main extends CrossLogging {

  object routes {

    val viewerDirectory = "milovanaViewer/target/UdashStatics/WebContent"
    val generatorDirectory = "milovanaGenerator/target/web"

    private def getExtensions(fileName: String): String = {
      val index = fileName.lastIndexOf('.')
      if (index != 0) {
        fileName.drop(index + 1)
      } else
        ""
    }

    private def withDefaultPage(path: Path) =
      if (Files.exists(path)) {
        if (Files.isDirectory(path)) {
          val index = path.resolve("index.html")
          if (Files.exists(index)) Some(index) else None
        } else Some(path)
      } else None

    private def deliverDirectory(dir: String, uri: Uri.Path) = {
      withDefaultPage(Paths.get(dir, uri.toString)) match {
        case Some(fullPath) =>
          val ext = getExtensions(fullPath.getFileName.toString)
          val mediaType = MediaTypes.forExtensionOption(ext).getOrElse(MediaTypes.`text/plain`)
          val gzPath = fullPath.getParent.resolve(fullPath.getFileName.toString + ".gz")
          val (realType, realPath) =
            if (!Files.isDirectory(fullPath) && Files.exists(gzPath))
              (mediaType.withComp(Gzipped), gzPath)
            else (mediaType, fullPath)

          val c: ContentType = ContentType(realType, () => HttpCharsets.`UTF-8`)
          val byteArray = Files.readAllBytes(realPath)
          HttpResponse(OK, entity = HttpEntity(c, byteArray))
        case None =>
          HttpResponse(NotFound)
      }
    }

    def generate = {
      logRequestResult("http-server") {
        get {
          entity(as[HttpRequest]) { requestData =>
            withPrecompressedMediaTypeSupport {
              pathEndOrSingleSlash {
                complete {
                  ""
                }
              } ~ path("viewer" / RemainingPath) { uri =>
                complete {
                  deliverDirectory(viewerDirectory, uri)
                }
              } ~ path("generator" / RemainingPath) { uri =>
                complete {
                  deliverDirectory(generatorDirectory, uri)
                }
              }
            }
          }
        }
      }
    }
  }

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem()
    implicit val executor = system.dispatcher
    implicit val materializer = ActorMaterializer()

    Http().bindAndHandle(routes.generate, "localhost", 8000)
  }

}