package io.gitlab.sklavedaniel.gaia.milovanagenerator

import io.gitlab.sklavedaniel.gaia.milovanagenerator.Milovana._

object Main {

  def main(argv: Array[String]): Unit = {
    val edgeCounter = Counter()
    val slapCounter = Counter()

    lazy val start = SimplePage.text("Do you want to edge?").button("YES!", slappingPhase)

    lazy val slappingPhase: Node = (edgeCounter += 1) :>
      SimplePage.text("Beat your balls 10 times!").go("Done!") :>
      SimplePage.text("And squeeze them good!").go("Done!") :>
      SimplePage.text("Want to edge more?").button("NO!", edgingPhase).button("YES!", slappingPhase)


    lazy val edgingPhase: Node = edgeCounter.copy(slapCounter) :> (edgeCounter -= 1) :>
      edgeCounter.thenDo(
        SimplePage.text("Edge!").go("Edged!") :>
          SimplePage.text("Take a short break.").button("Done", edgingPhase)
      ) :> slappingPhase2

    lazy val slappingPhase2: Node = (slapCounter -= 1) :>
      slapCounter.thenDo(
        SimplePage.text("Slap your balls 10 time!").go("Done!") :>
          SimplePage.text("I hope that hurt.").button("Yes", slappingPhase2)
      ) :> SimplePage.text("See you again soon!")

    println(start.generate())

  }
}
