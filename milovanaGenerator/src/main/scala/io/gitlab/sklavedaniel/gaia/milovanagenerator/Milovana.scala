package io.gitlab.sklavedaniel.gaia.milovanagenerator

import language.implicitConversions
import scala.collection.mutable
import com.thoughtworks.each.Monadic.{monadic, _}

import scalaz.Free.Trampoline
import scalaz.std.option._
import scalaz.std.list._

object Milovana {

  class Translator {
    val translated = new mutable.HashMap[Translatable, Int]()
    val source = new mutable.StringBuilder()
    val initSource = new mutable.StringBuilder()

    def translate(node: Translatable)(translation: Int => Trampoline[Unit]) = monadic[Trampoline] {
      translated.get(node) match {
        case Some(result) => result
        case None =>
          val i = translated.size
          translated.put(node, i)
          translation(i).each
          i
      }

    }

    override def toString = initSource.toString + source.toString + "start#goto(target:0#)\n"
  }

  sealed trait Translatable {
    def translate(translator: Translator): Trampoline[Int]
  }

  sealed trait Node extends Translatable {
    def generate() = {
      val translator = new Translator()
      translate(translator).run
      translator.toString
    }

  }

  sealed trait DelayStyle

  case object DelaySecret extends DelayStyle {
    override def toString = "secret"
  }

  case object DelayHidden extends DelayStyle {
    override def toString = "hidden"
  }

  case object DelayVisible extends DelayStyle {
    override def toString = "visible"
  }

  class Delay(time: Int, target: => Node, style: DelayStyle = DelayVisible) {
    def translate(translator: Translator) = monadic[Trampoline] {
      val id: Int = target.translate(translator).each
      s"delay(time:${time}sec,target:$id#,style:$style)"
    }
  }

  object Delay {
    def apply(time: Int, target: => Node, style: DelayStyle = DelayVisible) = new Delay(time, target, style)
  }

  class Button(text: String, target: => Node) {
    def translate(pos: Int, translator: Translator) = monadic[Trampoline] {
      val id = target.translate(translator).each
      s"""cap$pos:"${escape(text)}",target$pos:$id#"""
    }
  }

  object Button {
    def apply(text: String, target: => Node) = new Button(text, target)
  }

  class Counter extends Translatable {
    def translate(translator: Translator) = monadic[Trampoline] {
      translator.translate(this) { id =>
        monadic[Trampoline] {
          translator.source.append(s"repeatset(target:$id#,count:0)\n$id#page()\n")
        }
      }.each
    }

    def :=(other: Int) = SetAction(this, other)

    def +=(other: Int) = ChangeAction(this, other)

    def -=(other: Int) = ChangeAction(this, -other)

    def move(to: Counter*) = Chainable { next =>
      val incCounters: Chainable = to.map(_ += 1: Chainable).reduce(_ :> _)
      lazy val tmp: Node = (this -= 1) :> this.elseDo((this += 1) :> next) :> incCounters :> tmp
      tmp
    }

    def copy(to: Counter*) =
      (TmpCounter := 0) :> this.move(TmpCounter :: to.toList: _*) :> TmpCounter.move(this)


  }

  val TmpCounter = Counter()

  object Counter {
    def apply() = new Counter

    implicit def toCondition(counter: Counter): Condition = Condition(counter)
  }

  sealed trait Action extends Chainable {
    self =>
    def translate(translator: Translator): String

    override def chain(target: => Node) = monadic[Trampoline] {
      new Node {
        override def translate(translator: Translator) = monadic[Trampoline] {
          translator.translate(this) { id =>
            monadic[Trampoline][Unit] {
              val targetId: Int = target.translate(translator).each
              val actionAttr = self.translate(translator)
              //translator.source.append(s"$id#{goto(target:$targetId#);$actionAttr\n")
              translator.source.append(s"$id#{page(action:delay(time:0sec,target:$targetId#,style:hidden));$actionAttr\n"): Unit
            }
          }.each
        }
      }
    }
  }

  def Chainable(f: Node => Node) = new Chainable {
    override def chain(node: => Node) = monadic[Trampoline] {
      f(node)
    }
  }

  case class SetAction(counter: Counter, value: Int) extends Action {
    def translate(translator: Translator) = s"repeatset(target:${counter.translate(translator).run}#,count:$value)"

  }

  case class ChangeAction(counter: Counter, delta: Int) extends Action {
    def translate(translator: Translator) = s"repeatadd(target:${counter.translate(translator).run}#,count:$delta)"
  }


  case class Condition(counter: Counter, nonNegative: Boolean = true) {
    def translate(self: String, translator: Translator): Unit = {
      val counterId = counter.translate(translator).run
      val tmp = if (nonNegative) {
        s"mustnot(self:$self#,action0:$counterId#)\n"
      } else {
        s"must(self:$self#,action0:$counterId#)\n"
      }
      translator.initSource.append(tmp)
    }

    def thenDo(node: => Node) = Select(!this)(When(this)(node))

    def elseDo(node: => Node) = Select(this)(When(!this)(node))

    def unary_! = copy(nonNegative = !nonNegative)
  }

  class When(val conditions: List[Condition], target: => Node) {
    def translate(self: String, translator: Translator) = monadic[Trampoline] {
      for (e <- conditions) {
        e.translate(self, translator)
      }
      val targetId: Int = target.translate(translator).each
      //translator.source.append(s"$self#goto(target:$targetId#)\n")
      translator.source.append(s"$self#page(action:delay(time:0sec,target:$targetId#,style:hidden))\n")
    }

    def getTarget = target
  }

  object When {
    def apply(conditions: Condition*)(target: => Node) = new When(conditions.toList, target)
  }

  case class Select(choices: List[When]) extends Node {
    override def translate(translator: Translator) = monadic[Trampoline] {
      @monadic[Trampoline]
      def helper(id: Int) = {
        for ((choice, i) <- choices.zipWithIndex) {
          choice.translate(id + "c" + i, translator).each
        }
        val unsets = choices.indices.map(i => s"action$i:${id}c$i#").mkString(",")
        //translator.source.append(s"""$id#{goto(target:range(from:0,to:${choices.size - 1},prefix:"${id}c"));unset($unsets)""" + "\n")
        translator.source.append(s"""$id#{page(action:delay(time:0sec,target:range(from:0,to:${choices.size - 1},prefix:"${id}c"),style:hidden));unset($unsets)""" + "\n"): Unit
      }

      translator.translate(this)(helper _).each
    }
  }

  object Select {
    def apply(choices: When*): Select = Select(choices.toList)

    def apply(choice: Condition*)(choices: When*): Chainable = Chainable { node =>
      Select(When(choice: _*)(node) :: choices.toList: _*)
    }
  }

  trait Chainable {
    self =>
    def :>(node: => Node) = chain(node).run

    def chain(node: => Node): Trampoline[Node]

    def :>(chainable: => Chainable): Chainable = new Chainable {
      override def chain(node: => Node) = monadic[Trampoline] {
        self.chain(chainable.chain(node).each).each
      }
    }
  }

  def Ite(conditions: Condition*)(thenCase: => Node, elseCase: => Node) =
    Select(When(conditions: _*)(thenCase) +: conditions.map(c => When(!c)(elseCase)): _*)

  def Switch(choices: When*)(elseCase: => Node): Node = choices match {
    case Nil => elseCase
    case a :: rest => Ite(a.conditions: _*)(a.getTarget, Switch(rest: _*)(elseCase))
  }

  case class SimplePage(text: Option[String] = None, image: Option[String] = None, sound: Option[String] = None,
    delay: Option[Delay] = None, buttons: List[Button] = Nil
  ) extends Node {
    def text(str: String): SimplePage = copy(text = Some(str))

    def image(str: String): SimplePage = copy(image = Some(str))

    def sound(str: String): SimplePage = copy(sound = Some(str))

    def delay(time: Int, target: => Node, style: DelayStyle = DelayVisible): SimplePage = copy(delay = Some(Delay(time, target, style)))

    def button(text: String, target: => Node): SimplePage = copy(buttons = buttons :+ Button(text, target))

    def go(text: String): Chainable = Chainable { node =>
      button(text, node)
    }

    def go(time: Int, style: DelayStyle = DelayVisible): Chainable = Chainable { node =>
      delay(time, node, style)
    }


    @monadic[Trampoline]
    def helper(id: Int, translator: Translator) = {
      val textAttr = text.map(s => List("text:\"" + escape(s) + '"')).getOrElse(Nil)
      val imageAttr = image.map(s => List("media:pic(id:\"" + escape(s) + "\")")).getOrElse(Nil)
      val soundAttr = sound.map(s => List("hidden:sound(id:\"" + escape(s) + "\")")).getOrElse(Nil)
      val delayAttr = for (x <- delay) yield
        List(x.translate(translator).each)
      val buttonsAttr = for ((b, i) <- buttons.zipWithIndex) yield
        b.translate(i, translator).each
      val actions = (delayAttr.getOrElse(Nil) :+ buttonsAttr.mkString("buttons(", ",", ")")).zipWithIndex.map(x => "e" + x._2 + ":" + x._1)
      val actionsAttr = List("action:vert(" + actions.mkString(",") + ")")
      val attrs = textAttr ++ imageAttr ++ soundAttr ++ actionsAttr
      val tmp = id + "#page(" + attrs.mkString(",") + ")\n"
      translator.source.append(tmp): Unit
    }

    @monadic[Trampoline]
    override def translate(translator: Translator) = {

      translator.translate(this)(x => helper(x, translator)).each

    }
  }

  object SimplePage extends SimplePage(None, None, None, None, Nil)

  def escape(str: String) = str.replace("&", "&amp;").replace("<", "lt;").replace(">", "&gt;").replace("'", "&apos;").replace("\"", "&quot;")


}

