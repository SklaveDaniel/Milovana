import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._

object Dependencies {
  val versionOfScala = "2.12.4"
  val udashVersion = "0.6.0"
  val udashJQueryVersion = "1.1.0"

  val milovanaInterpreterDeps = Def.setting(Seq(
    "org.parboiled" %%% "parboiled" % "2.1.4",
    "com.chuusai" %%% "shapeless" % "2.3.2",
    "com.lihaoyi" %%% "upickle" % "0.5.1"
  ))
  val milovanaViewerDeps = Def.setting(Seq(
    "io.udash" %%% "udash-core-shared" % udashVersion,
    "io.udash" %%% "udash-rpc-shared" % udashVersion,
    "io.udash" %%% "udash-core-frontend" % udashVersion,
    "io.udash" %%% "udash-rpc-frontend" % udashVersion,
    "io.udash" %%% "udash-jquery" % udashJQueryVersion,
    "org.scala-js" %%% "scalajs-dom" % "0.9.4",
    "io.monix" %%% "monix" % "2.3.3"
  ))
  val milovanaGeneratorDeps = Def.setting(Seq(
    "com.thoughtworks.each" %% "each" % "latest.release"
  ))
  val backendDeps = Def.setting(Seq(
    "io.udash" %% "udash-rpc-backend" % udashVersion,
    "com.typesafe.akka" %% "akka-actor" % "2.5.10",
    "com.typesafe.akka" %% "akka-http" % "10.1.0-RC2",
    "com.typesafe.akka" %% "akka-stream" % "2.5.10"
  ))

}
