import java.io.File

import sbt.Keys.sourceDirectory

import scala.sys.process.Process

import language.postfixOps

name := "Gaia"

cancelable in Global := true

inThisBuild(Seq(
  version := "0.1.0-SNAPSHOT",
  scalaVersion := Dependencies.versionOfScala,
  organization := "io.gitlab.sklavedaniel.gaia",
  scalacOptions ++= Seq(
    "-feature",
    "-deprecation",
    "-unchecked",
    "-Xfuture",
    "-Xcheckinit",
    "-Xlint:_,-missing-interpolator,-adapted-args",
  ),
) ++ addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full))

watchSources ++= Seq(baseDirectory.value / "milovanaViewer" / "src" / "main" / "assets", baseDirectory.value / "milovanaViewer" / "src" / "main" / "sass")

val copyAssets = taskKey[Unit]("Copies all assets to the target directory.")
val compileStatics = taskKey[Unit](
  "Compiles JavaScript files and copies all assets to the target directory."
)
val compileAndOptimizeStatics = taskKey[Unit](
  "Compiles and optimizes JavaScript files and copies all assets to the target directory."
)

val commonSettings = Seq(
  moduleName := "gaia-" + moduleName.value,
)
val commonJSSettings = Seq(
  Compile / emitSourceMaps := true,
)


lazy val gaia = project.in(file("."))
  .aggregate(milovanaInterpreterJS, milovanaInterpreterJVM, milovanaViewer, milovanaGenerator, server)
  .dependsOn(server)
  .settings(
    publishArtifact := false,
    Compile / mainClass := Some("io.gitlab.sklavedaniel.gaia.server.Main")
  )

lazy val milovanaInterpreter = crossProject
  .crossType(CrossType.Pure).in(file("shared"))
  .settings(commonSettings)
  .jsSettings(commonJSSettings)
  .settings(
    libraryDependencies ++= Dependencies.milovanaInterpreterDeps.value
  )
lazy val milovanaInterpreterJVM = milovanaInterpreter.jvm
lazy val milovanaInterpreterJS = milovanaInterpreter.js

lazy val milovanaViewer = project.in(file("milovanaViewer"))
  .enablePlugins(ScalaJSPlugin)
  .dependsOn(milovanaInterpreterJS % "test->test;compile->compile")
  .settings(commonSettings)
  .settings(commonJSSettings)
  .settings(
    libraryDependencies ++= Dependencies.milovanaViewerDeps.value,
    Compile / mainClass := Some("io.gitlab.sklavedaniel.gaia.milovanaviewer.Main"),
    scalaJSUseMainModuleInitializer := true,
    copyAssets := {
      IO.delete(recursiveListFiles(target.value / "UdashStatics/WebContent").filter(_.getName.endsWith(".gz")))
      IO.copyDirectory(
        sourceDirectory.value / "main/assets",
        target.value / "UdashStatics/WebContent/assets"
      )
      IO.copyFile(
        sourceDirectory.value / "main/assets/index.html",
        target.value / "UdashStatics/WebContent/index.html"
      )
      val gzteases = (sourceDirectory.value / "main/assets/teases").listFiles().toList.filter(_.getName.endsWith(".gz")).map(_.getName)
      IO.touch(gzteases.map(s => target.value / "UdashStatics/WebContent/assets/teases" / s.substring(0, s.length - 3)))

      Process("sass" :: (sourceDirectory.value / "main/sass/style.sass").getPath + ":" + (target.value / "UdashStatics/WebContent/assets/style.css").getPath :: Nil) !
    },
    compileStatics := {
      gzip(target.value / "UdashStatics/WebContent")
    },
    compileStatics := compileStatics.dependsOn(
      Compile / fastOptJS, Compile / copyAssets
    ).value,
    compileAndOptimizeStatics := {
      gzip(target.value / "UdashStatics/WebContent")
    },
    compileAndOptimizeStatics := compileAndOptimizeStatics.dependsOn(
      Compile / fullOptJS, Compile / copyAssets
    ).value,
    Compile / fastOptJS / artifactPath :=
      (Compile / fastOptJS / target).value /
        "UdashStatics" / "WebContent" / "scripts" / "frontend.js",
    Compile / fullOptJS / artifactPath :=
      (Compile / fullOptJS / target).value /
        "UdashStatics" / "WebContent" / "scripts" / "frontend.js",
    Compile / packageJSDependencies / artifactPath :=
      (Compile / packageJSDependencies / target).value /
        "UdashStatics" / "WebContent" / "scripts" / "frontend-deps.js",
    Compile / packageMinifiedJSDependencies / artifactPath :=
      (Compile / packageMinifiedJSDependencies / target).value /
        "UdashStatics" / "WebContent" / "scripts" / "frontend-deps.js"
  )

lazy val milovanaGenerator = project.in(file("milovanaGenerator"))
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Dependencies.milovanaGeneratorDeps.value,
    Compile / mainClass := Some("io.gitlab.sklavedaniel.gaia.milovanagenerator.Main"),
  )

lazy val server = project.in(file("server"))
  .dependsOn(milovanaInterpreterJVM % "test->test;compile->compile")
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Dependencies.backendDeps.value,
    Compile / mainClass := Some("io.gitlab.sklavedaniel.gaia.server.Main"),
  )

lazy val utilities = project.in(file("utilities"))
  .dependsOn(milovanaInterpreterJVM % "test->test;compile->compile")
  .settings(commonSettings)

def recursiveListFiles(f: File): Stream[File] = {
  if (f.exists() && f.isDirectory) {
    val these = f.listFiles.toStream
    these.filter(_.isFile) ++ these.flatMap(recursiveListFiles)
  } else {
    Stream.empty
  }
}

def gzip(file: File): Unit = {
  for {
    f <- recursiveListFiles(file)
    if !f.getName.endsWith(".gz")
    if f.isFile
    if !new File(f.getParentFile, f.getName + ".gz").exists
  } {
    Process("gzip" :: "-k" :: f.getPath :: Nil) !
  }
}

