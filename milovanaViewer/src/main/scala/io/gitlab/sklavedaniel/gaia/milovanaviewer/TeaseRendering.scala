package io.gitlab.sklavedaniel.gaia.milovanaviewer

import io.gitlab.sklavedaniel.gaia.shared.BaseParser._
import io.gitlab.sklavedaniel.gaia.shared.ScriptInterpreter._
import io.udash._
import io.udash.bindings.modifiers.Binding
import org.scalajs.dom.ext.AjaxException
import org.scalajs.dom.raw.Event

import scala.annotation.tailrec
import scala.scalajs.js
import scala.util.{Failure, Success, Try}

import scalatags.JsDom.all._

object TeaseRendering {

  sealed trait TimerMode

  case object EnableTimers extends TimerMode

  case object SkipTimers extends TimerMode

  case object DisableTimers extends TimerMode

  case class Flags(timerMode: TimerMode = EnableTimers, strictChecks: Boolean = false, teaseSource: TeaseSource = ModifiedSource)

  object Flags extends HasModelPropertyCreator[Flags]

  sealed trait TeaseSource

  case object OriginalSource extends TeaseSource

  case object ModifiedSource extends TeaseSource

  case object ProvidedSource extends TeaseSource

  trait TeasePresenter {
    def performAction(target: Target, id: String, configuration: Configuration, flags: Flags, tease: Script): Unit

    def restart(pid: String, flags: Flags): Unit

    def exit(): Unit
  }

  case class TeaseModel(value: Option[(String, Try[(Script, Option[TeaseInfo], String => String)], Configuration)] = None,
    timeout: Option[Int] = None, delay: Option[Double] = None, flags: Flags = Flags()
  )

  object TeaseModel extends HasModelPropertyCreator[TeaseModel]


  class TeaseView(model: ModelProperty[TeaseModel], presenter: TeasePresenter) extends FinalView {

    override def getTemplate: Modifier = {
      produceWithNested(model.subProp(_.value)) {
        (currentModel, nested) =>
          div(`class` := "tease")(
            currentModel match {
              case Some((pid, Success((t, info, resolve)), configuration)) =>
                div(`class` := "tease-success")(
                  div(`class` := "tease-header")(renderTitle(pid, info), renderMenu(pid, model.subProp(_.flags).get)),
                  div(`class` := "tease-content")(
                    renderTease(pid, configuration, resolve, model.subProp(_.flags).get, t, nested)
                  )
                )
              case Some((pid, Failure(e), _)) =>
                div(`class` := "tease-failure")(
                  div(`class` := "tease-header")(renderTitle(pid, None), renderMenu(pid, model.subProp(_.flags).get)),
                  div(`class` := "tease-content")(
                    div(`class` := "tease-main")(
                      p(`class` := "tease-text tease-warning")("Error occured during loading of tease"),
                      p(`class` := "tease-text")(strong("Error type: "), e.getClass.getName),
                      getMessage(e),
                      div(`class` := "tease-stacktrace")(e.getStackTrace.map(st => p(`class` := "tease-stacktrace-entry")(st.toString)))
                    ),
                    div(`class` := "tease-sidebar")()
                  )
                )
              case None =>
                div(`class` := "tease-loading")(p(`class` := "tease-text")("Loading tease..."))
            }
          ).render
      }
    }

    def renderTitle(pid: String, info: Option[TeaseInfo]) = info match {
      case Some(tmp) =>
        frag(
          h1(tmp.title),
          div(`class` := "tease-info")(a(href := s"https://milovana.com/forum/memberlist.php?mode=viewprofile&u=${tmp.authorId}")(tmp.author)),
        )
      case None =>
        h1("Tease Id:" + pid)
    }

    def renderMenu(pid: String, flags: Flags) = div(`class` := "tease-menu")(
      p("menu"),
      ul(
        li(onclick :+= ((_: Event) => {
          val d = js.Dynamic.global.document.documentElement
          if (!js.isUndefined(d.requestFullscreen)) d.requestFullscreen()
          else if (!js.isUndefined(d.webkitRequestFullscreen)) d.webkitRequestFullscreen()
          else if (!js.isUndefined(d.mozRequestFullScreen)) d.mozRequestFullScreen()
          else if (!js.isUndefined(d.msRequestFullscreen)) d.msRequestFullscreen()
        }))(span(`class` := "icon")("fullscreen"), "fullscreen"),
        li(onclick :+= ((_: Event) => {
          presenter.restart(pid, flags)
        }))(span(`class` := "icon")("replay"), "restart"),
        li(onclick :+= ((_: Event) => {
          presenter.exit()
        }))(span(`class` := "icon")(raw("&#xE879")), "exit")
      )
    )

    def getMessage(e: Throwable) = e match {
      case e: AjaxException if e.isTimeout =>
        frag(
          p(`class` := "tease-text tease-error-message")(strong("Message: "), "Timeout occured."),
          p(`class` := "tease-text tease-error-message")(strong("URI: "), e.xhr.responseURL.toString)
        )
      case e: AjaxException =>
        frag(
          p(`class` := "tease-text tease-error-message")(strong("Message: "), e.xhr.status.toString, ": ", e.xhr.statusText),
          p(`class` := "tease-text tease-error-message")(strong("URI: "), e.xhr.responseURL.toString)
        )
      case e2 =>
        frag(Option(e2.getLocalizedMessage).orElse(Option(e2.getMessage)).map(x => p(`class` := "tease-text tease-error-message")(strong("Message: "), x)))
    }

    def renderTease(id: String, configuration: Configuration, resolve: String => String, flags: Flags, tease: Script, nested: Binding => Binding) =
      configuration.page match {
        case Some(page) =>
          tease.script.get(page) match {
            case Some(c) => renderControlLocation(id, configuration, resolve, flags, tease, nested)(c)
            case None =>
              frag(
                div(`class` := "tease-main")(
                  p(`class` := "tease-text tease-warning")("Page ", em(configuration.page), " does not exist.")
                ),
                div(`class` := "tease-sidebar")()
              )
          }
        case None =>
          frag(
            div(`class` := "tease-main")(
              p(`class` := "tease-text tease-warning")("No more pages to select."),
              p(`class` := "tease-text")("This means that some button or timer refered to no page or that all pages in a range are marked as visited.")
            ),
            div(`class` := "tease-sidebar")()
          )
      }

    def renderControlLocation(id: String, configuration: Configuration, resolve: String => String, flags: Flags, tease: Script, nested: Binding => Binding)(location: ControlLocation) = {
      location match {
        case Goto(target, _, _) => frag(div(`class` := "tease-main")(), div(`class` := "tease-sidebar")(p(button(onclick :+= { (_: Event) =>
          presenter.performAction(target, id, configuration, flags, tease)
        })("Goto"))))
        case Page(mainContent, sidebarContent, sound, delay, _, _) =>
          frag(
            div(`class` := "tease-main")(
              mainContent.map(renderPage(id, configuration, resolve, flags, tease, nested))
            ),
            div(`class` := "tease-sidebar")(
              sidebarContent.map(renderPage(id, configuration, resolve, flags, tease, nested)),
              if (flags.timerMode != EnableTimers) {
                div(`class` := "tease-debug")(
                  delay.map { d =>
                    p(button(
                      onclick :+= { (_: Event) =>
                        presenter.performAction(d.target, id, configuration, flags, tease)
                      }
                    )(
                      "skip timer"
                    ))
                  }
                )
              } else frag()
            ),
            sound.map(s => audio(attr("autoplay") := "true")(
              for (_ <- 0 until s.loop) yield
                source(src := resolve(s.id.toString))())
            )
          )
      }
    }

    def renderPage(id: String, configuration: Configuration, resolve: String => String, flags: Flags, tease: Script, nested: Binding => Binding)(content: PageContent): Frag = {
      content match {
        case VerticalBox(l) =>
          div(`class` := "vertical-box")(l.map(renderPage(id, configuration, resolve, flags, tease, nested)))
        case HorizontalBox(l) =>
          div(`class` := "horizonal-box")(l.map(renderPage(id, configuration, resolve, flags, tease, nested)))
        case HTMLContent(l) =>
          div(renderHTML(l))
        case Picture(uri) =>
          p(img(src := resolve(uri.toString))())
        case Button(c, t) =>
          p(button {
            onclick :+= { (_: Event) =>
              presenter.performAction(t, id, configuration, flags, tease)
            }
          }(renderHTML(c)))
        case Timer(TimerStyleVisible) =>
          p(`class` := "tease-timer")(nested(produce(model.subProp(_.delay)) { d =>
            span(d.map(x => toTimeString(x)).getOrElse("None"): String).render
          }))
        case Timer(TimerStyleSecret) =>
          p(`class` := "tease-timer")(
            span("??:??:??").render
          )
        case _ => frag()
      }
    }

    def toTimeString(time: Double): String = {
      val rounded = time.round
      val sec = rounded % 60
      val min = (rounded / 60) % 60
      val h = rounded / 60 / 60
      f"$h%02d:$min%02d:$sec%02d"
    }


    def renderHTML(content: List[MHTMLNode]): Frag = {
      @tailrec
      def recurse(result: List[Frag], string: Option[String], content: List[MHTMLNode]): List[Frag] = content match {
        case Nil =>
          (string match {
            case Some(oldString) => insertURLLinks(oldString) :: result
            case None => result
          }).reverse
        case MHTMLText(moreString) :: rest =>
          string match {
            case Some(oldString) =>
              recurse(result, Some(oldString + moreString), rest)
            case None =>
              recurse(result, Some(moreString), rest)
          }
        case MHTMLRef(n) :: rest =>
          string match {
            case Some(oldString) =>
              recurse(result, Some(oldString + unescape(n)), rest)
            case None =>
              recurse(result, Some(unescape(n)), rest)
          }
        case MHTMLTag(n, attrs, c) :: rest =>
          val r = n match {
            case "FONT" | "font" =>
              val tmp = attrs.toMap.map(x => x._1.toLowerCase -> unescape(x._2))
              span(
                tmp.get("SIZE").orElse(tmp.get("size")).map(x => fontSize := x + "px"),
                tmp.get("FACE").orElse(tmp.get("face")).map {
                  case "FontSerif" => `class` := "font-serif"
                  case "FontSans" => `class` := "font-sans"
                  case "FontTypewriter" => `class` := "font-typewriter"
                  case x => fontFamily := x
                },
                tmp.get("COLOR").orElse(tmp.get("color")).map(x => color := x)
              )(
                renderHTML(c)
              )
            case "P" | "p" => p(renderHTML(c))
            case "B" | "b" => b(renderHTML(c))
            case "I" | "i" => i(renderHTML(c))
            case "U" | "u" => u(renderHTML(c))
            case "BR" | "br" => br
            case "TEXTFORMAT" | "textformat" => renderHTML(c)
            case n => throw new NotImplementedError(s"HTML Tag $n not implemented.")
          }
          string match {
            case Some(oldString) => recurse(r :: insertURLLinks(oldString) :: result, None, rest)
            case None => recurse(r :: result, None, rest)
          }
      }

      frag(recurse(Nil, None, content))
    }

    val urlRegExp = """(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)""".r

    def insertURLLinks(string: String) = {
      val plain = urlRegExp.split(string).map(stringFrag).toList
      val urls = urlRegExp.findAllMatchIn(string).toList.map { m =>
        val prefix = if (m.group(1) == "") "http://" else ""
        a(`class` := "autolink")(href := prefix + m.group(0))(m.group(0))
      }
      frag(plain.zipAll(urls, frag(), frag()).flatMap(x => List(x._1, x._2)))
    }

    def unescape(data: List[MHTMLTextNode]): String = data.collect {
      case MHTMLText(t) => t
      case MHTMLRef(n) => unescape(n)
    }.mkString("")

    def unescape(data: String): String = data match {
      case "amp" => "&"
      case "quot" => "\""
      case "apos" => "'"
      case "gt" => ">"
      case "lt" => "<"
      case n => throw new NotImplementedError(s"HTML Reference $n not implemented.")
    }

  }

}
