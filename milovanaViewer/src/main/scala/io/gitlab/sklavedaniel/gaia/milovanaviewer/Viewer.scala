package io.gitlab.sklavedaniel.gaia.milovanaviewer

import java.util.Random

import io.gitlab.sklavedaniel.gaia.milovanaviewer.PropertyOps._
import io.gitlab.sklavedaniel.gaia.milovanaviewer.TeaseRendering.{TeaseModel, TeaseView, _}
import io.gitlab.sklavedaniel.gaia.shared.BaseParser._
import io.gitlab.sklavedaniel.gaia.shared.ScriptInterpreter._
import io.gitlab.sklavedaniel.gaia.shared.{ScriptInterpreter, ScriptParser}
import io.udash._
import io.udash.properties.Valid
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom
import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.raw.{Event, File, FileReader}
import upickle.default._
import scalatags.JsDom.all._
import scala.scalajs.js.{Date, URIUtils}
import scala.util.{Failure, Success, Try}

class Viewer(val metadata: Option[Map[String, TeaseInfo]]) {
  val random = new Random()

  val uploadAuthor: Property[String] = Property("")
  uploadAuthor.addValidator { s =>
    if (s.matches("\\d+")) {
      Valid
    } else {
      Invalid("Author ID may only contain digits.")
    }
  }
  val uploadFile: Property[Option[File]] = Property(None)
  uploadFile.addValidator { f =>
    f match {
      case None => Invalid("")
      case Some(_) => Valid
    }
  }

  sealed trait RoutingState extends State with FinalState {
    override type HierarchyRoot = RoutingState
  }

  object InitState extends RoutingState {
    override type HierarchyRoot = RoutingState

    override def parentState = None
  }


  case class TeaseState(id: String, configuration: Configuration, flags: Flags) extends RoutingState with FinalState {
    override type HierarchyRoot = RoutingState

    override def parentState = None
  }

  object MyRoutingRegistry extends RoutingRegistry[RoutingState] {
    val Teasergx = "tease/([^/,]+)".r
    val Pagergx = "tease/([^/,]+)/([^/,]*)".r
    val Setrgx = "tease/([^/,]+)/([^/,]*)/([^/]*)".r
    val Optionsrgx = "tease/([^/,]+)/([^/,]*)/([^/]*)/([^/]*)".r

    override def matchUrl(url: Url): RoutingState = {
      def matchFlags(flags: String) = {
        val tmp = flags.split(',').toSet
        Flags(if (tmp.contains("skipTimer")) SkipTimers else if (tmp.contains("noTimer")) DisableTimers else EnableTimers, tmp.contains("strict"),
          if (tmp.contains("mod")) ModifiedSource else if (tmp.contains("prov")) ProvidedSource else OriginalSource)
      }

      def getSetPages(s: String) = s match {
        case "" => Map[String, Int]()
        case _ => s.split(',').map { s2 =>
          s2.split(":") match {
            case Array(a, b) => a -> b.toInt
            case Array(a) => a -> 1
          }
        }.toMap
      }

      url.value match {
        case Teasergx(id) => TeaseState(id, Configuration(Some(""), Map()), Flags())
        case Pagergx(id, "#") => TeaseState(id, Configuration(None, Map()), Flags())
        case Pagergx(id, page) => TeaseState(id, Configuration(Some(page), Map()), Flags())
        case Setrgx(id, "#", counters) => TeaseState(id, Configuration(None, getSetPages(counters)), Flags())
        case Setrgx(id, page, counters) => TeaseState(id, Configuration(Some(page), getSetPages(counters)), Flags())
        case Optionsrgx(id, "#", counters, flags) =>
          TeaseState(id, Configuration(None, getSetPages(counters)), matchFlags(flags))
        case Optionsrgx(id, page, counters, flags) =>
          TeaseState(id, Configuration(Some(page), getSetPages(counters)), matchFlags(flags))
        case _ => InitState
      }
    }

    override def matchState(state: RoutingState): Url = {
      def flagToString(flag: Boolean, string: String) = if (flag) List(string) else Nil

      state match {
        case InitState => Url("")
        case TeaseState(id, Configuration(page, counters), flags) =>
          val s1 = page.getOrElse("#")
          val s2 = counters.toSeq.flatMap {
            case (c, 0) => None
            case (c, 1) => Some(c)
            case (c, i) => Some(c + ":" + i)
          }.sorted.mkString(",")
          val s3 = ((flags.timerMode match {
            case DisableTimers => List("noTimer")
            case SkipTimers => List("skipTimer")
            case EnableTimers => Nil
          }) ++ flagToString(flags.strictChecks, "strict") ++ (flags.teaseSource match {
            case ModifiedSource => List("mod")
            case ProvidedSource => List("prov")
            case OriginalSource => Nil
          })).mkString(",")
          Url(s"tease/$id/$s1/$s2/$s3")
      }
    }
  }

  object MyViewFactory extends ViewFactoryRegistry[RoutingState] {
    def matchStateToResolver(state: RoutingState): ViewFactory[_ <: RoutingState] =
      state match {
        case InitState => InitViewFactory
        case TeaseState(_, _, _) => TeaseViewFactory
      }
  }

  case class InitModel(id: String, flags: Flags)

  object InitModel extends HasModelPropertyCreator[InitModel]

  case object InitViewFactory extends ViewFactory[InitState.type] {
    override def create(): (View, Presenter[InitState.type]) = {
      val model = ModelProperty(new InitModel("664", Flags()))
      model.subProp(_.id).addValidator { s =>
        if (s.matches("\\d+")) {
          Valid
        } else {
          Invalid("Tease ID may only contain digits.")
        }
      }
      val presenter = new InitPresenter(model)
      val view = new InitView(model, presenter)
      (view, presenter)
    }
  }


  class InitPresenter(model: ModelProperty[InitModel]) extends Presenter[InitState.type] {
    override def handleState(state: InitState.type): Unit = {}
  }

  class InitView(model: ModelProperty[InitModel], presenter: InitPresenter) extends FinalView {

    override def getTemplate: Modifier = {
      val idValid = model.subProp(_.id).valid.is(Valid)
      val authorValid = uploadAuthor.valid.is(Valid)
      val uploadValid = uploadFile.valid.is(Valid)
      val provided = model.subProp(_.flags.teaseSource).is(ProvidedSource)
      val canStart = idValid && (provided ==> (authorValid && uploadValid))

      div(`class` := "welcome")(
        p("Please enter the ID of a Webtease:"),
        p(
          TextInput.debounced(model.subProp(_.id))((`class` := "invalid").attrIfNot(idValid)),
          " ",
          button(
            onclick :+= ((_: Event) => {
              MyApp.goTo(TeaseState(model.get.id, ScriptInterpreter.initial, model.get.flags))
            }),
            (disabled := "disabled").attrIfNot(canStart)
          )("Start")
        ),
        div(`class` := "flags")(
          p(label(Checkbox(model.subProp(_.flags.strictChecks)), "check tease for issues")),
          p("Timer mode: ", TypedSelect(model.subProp(_.flags.timerMode),
            Seq(EnableTimers -> frag("enabled"), SkipTimers -> frag("skipable"), DisableTimers -> frag("disabled")))
          ),
          p("Tease script: ", TypedSelect(model.subProp(_.flags.teaseSource),
            Seq(OriginalSource -> frag("original"), ModifiedSource -> frag("modified"), ProvidedSource -> frag("upload")))
          )
        ),
        produceWithNested(model.subProp(_.flags.teaseSource)) { (x, nested) =>
          (if (x == ProvidedSource) {
            div(`class` := "script-upload")(
              p(FileInput("", Property(false), uploadFile.transformToSeq(_.toList, _.headOption))(
                nested((`class` := "invalid").attrIfNot(uploadValid))
              )),
              p("Author ID: ", TextInput.debounced(uploadAuthor)(nested((`class` := "invalid").attrIfNot(authorValid))))
            )
          } else div(`class` := "script-upload-disabled")()).render
        },
        p(`class` := "note")(strong("Note: "), "If you are using a mobile device, make sure your browser does not block audio autoplay, if you want to hear sounds.")
      )
    }
  }

  case object TeaseViewFactory extends ViewFactory[TeaseState] {
    override def create(): (View, Presenter[TeaseState]) = {
      val model = ModelProperty(new TeaseModel())
      val presenter = new MyTeasePresenter(model)
      val view = new TeaseView(model, presenter)
      (view, presenter)
    }
  }

  class MyTeasePresenter(model: ModelProperty[TeaseModel]) extends Presenter[TeaseState] with TeasePresenter {
    override def handleState(state: TeaseState): Unit = {
      model.get.timeout.foreach(dom.window.clearInterval)
      model.get.value match {
        case Some((id, Success(tease), _)) if id == state.id =>
          setModel(state, Success(tease))
        case _ =>
          println("loading " + dom.window.location)
          model.set(TeaseModel(flags = state.flags))
          load(state) { r =>
            val tease = r.map { x =>
              assert(!state.flags.strictChecks || (x._1.unreachable.isEmpty && x._1.undefined.isEmpty && x._1.notargets.isEmpty),
                s"Additional checks failed:\nUndefined pages: ${x._1.undefined}\nUnreachable pages: ${x._1.unreachable}\nPages with actions without targets: ${x._1.notargets}")
              x
            }
            setModel(state, tease)
          }
      }
    }

    override def onClose(): Unit = {
      model.get.timeout.foreach(dom.window.clearInterval)
    }

    val TeaseInfoRgx = """<div class="title">([^<]*) by <a href="webteases/[?#]author=(\d+)"[^>]*>([^<]*)</a></div>""".r
    val TeaseNotFoundRgx = """<h1>Tease not found.</h1>|<h1>This tease is invisible.</h1>""".r

    def load(state: TeaseState)(callback: Try[(Script, Option[TeaseInfo], String => String)] => Any): Unit = state.flags.teaseSource match {
      case ModifiedSource =>
        metadata match {
          case Some(map) =>
            map.get(state.id) match {
              case Some(ti) =>
                Ajax.get(s"assets/teases/${URIUtils.encodeURIComponent(state.id)}.txt").onComplete { result =>
                  val info = result.flatMap { r =>
                    Try(read[Script](r.responseText)).map { script =>
                      (script, Some(ti), resolve(state.id, ti))
                    }
                  }
                  callback(info)
                }
              case None => loadOriginal(state)(callback)
            }
          case None => loadOriginal(state)(callback)
        }
      case ProvidedSource =>
        Try(uploadFile.get.get) match {
          case Success(f) =>
            val reader = new FileReader()
            reader.onload = _ => {
              val info = ScriptParser.parse(reader.result.asInstanceOf[String]).map(ScriptInterpreter.toScript).map {
                script =>
                  (script, None, resolve(state.id, uploadAuthor.get))
              }
              callback(info)
            }
            reader.onerror = _ => {
              callback(Failure(new RuntimeException(s"Could not load file $f: ${
                reader.error.name
              }")))
            }
            reader.readAsText(f)
          case Failure(e) => callback(Failure(new RuntimeException("No tease has been uploaded.")))
        }
      case OriginalSource => loadOriginal(state)(callback)
    }

    def loadOriginal(state: TeaseState)(callback: Try[(Script, Option[TeaseInfo], String => String)] => Any): Unit = {
      val scripturi = s"https://milovana.com/webteases/getscript.php?id=${URIUtils.encodeURIComponent(state.id)}"
      val infouri = s"https://milovana.com/webteases/showflash.php?id=${URIUtils.encodeURIComponent(state.id)}"
      Ajax.get(s"https://cors-anywhere.herokuapp.com/$scripturi").zip(Ajax.get(s"https://cors-anywhere.herokuapp.com/$infouri")).onComplete { r =>
        val info = r.flatMap { res =>
          TeaseInfoRgx.findFirstMatchIn(res._2.responseText) match {
            case Some(m) =>
              val Seq(title, authorId, author) = m.subgroups
              val info = TeaseInfo(title, author, authorId, "milovana.com")
              val script = ScriptParser.parse(res._1.responseText).map(ScriptInterpreter.toScript)
              script.map(s => (s, Some(info), resolve(state.id, info)))
            case _ =>
              TeaseNotFoundRgx.findFirstIn(res._2.responseText) match {
                case Some(_) => Failure(new RuntimeException("Could not find tease."))
                case None => Failure(new RuntimeException("Could not parse tease meta-information."))
              }
          }

        }
        callback(info)
      }
    }

    def resolve(id: String, info: TeaseInfo): String => String = resolve(id, info.authorId)

    def resolve(id: String, authorId: String): String => String = (file: String) => {
      val append = if (file.contains("*")) s"&time=${System.currentTimeMillis}" else ""
      s"https://milovana.com/media/get.php?folder=$authorId/$id&name=$file$append"
    }

    private def setModel(state: TeaseState, tease: Try[(Script, Option[TeaseInfo], String => String)]): Unit = tease match {
      case Success((t, _, r)) =>
        state.configuration.page match {
          case Some("") =>
            model.set(TeaseModel(Some(state.id, tease, state.configuration), flags = state.flags), force = true)
            performAction(SingleTarget("start"), state.id, state.configuration, state.flags, t)
          case Some(p) =>
            t.script.get(p) match {
              case Some(cl) =>
                cl match {
                  case Goto(target, _, _) if state.flags.timerMode != DisableTimers =>
                    performAction(target, state.id, state.configuration, state.flags, t)
                  case Page(_, _, _, Some(Delay(RandomNumber(0.0, 0.0), target)), _, _) if state.flags.timerMode != DisableTimers =>
                    performAction(target, state.id, state.configuration, state.flags, t)
                  case Page(_, _, _, Some(Delay(RandomNumber(min, max), target)), _, _) =>
                    val value = min + random.nextDouble * (max - min)
                    val time = Date.now() / 1000
                    lazy val timeout: Int = dom.window.setInterval(() => {
                      val d = Math.max(value + time - Date.now() / 1000, 0.0).round
                      model.subProp(_.delay).set(Some(d))
                      if (d == 0.0) {
                        dom.window.clearInterval(timeout)
                        if (state.flags.timerMode != DisableTimers) {
                          performAction(target, state.id, state.configuration, state.flags, t)
                        }
                      }
                    }, 1000)
                    model.set(TeaseModel(Some(state.id, tease, state.configuration), Some(timeout), Some(value), state.flags), force = true)
                  case _ =>
                    model.set(TeaseModel(Some(state.id, tease, state.configuration), flags = state.flags), force = true)
                }
              case None =>
                model.set(TeaseModel(Some(state.id, tease, state.configuration), flags = state.flags), force = true)
            }
          case None =>
            model.set(TeaseModel(Some(state.id, tease, state.configuration), flags = state.flags), force = true)
        }
      case Failure(e) =>
        model.set(TeaseModel(Some(state.id, tease, state.configuration), flags = state.flags), force = true)
    }

    def performAction(target: Target, id: String, configuration: Configuration, flags: Flags, tease: Script): Unit = {
      val newConfiguration = ScriptInterpreter.next(target, configuration, tease, random)
      val newState = TeaseState(id, newConfiguration, flags)
      if (configuration == newConfiguration) {
        handleState(newState)
      } else {
        MyApp.goTo(newState)
      }
    }

    override def exit(): Unit = {
      MyApp.goTo(InitState)
    }

    override def restart(id: String, flags: Flags): Unit = {
      MyApp.goTo(TeaseState(id, ScriptInterpreter.initial, flags))
    }
  }

  object MyApp extends Application[RoutingState](MyRoutingRegistry, MyViewFactory)

}
