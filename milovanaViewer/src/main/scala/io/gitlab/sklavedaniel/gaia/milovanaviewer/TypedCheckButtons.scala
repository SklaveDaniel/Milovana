package io.gitlab.sklavedaniel.gaia.milovanaviewer

import io.udash._
import org.scalajs.dom.{Event, html}

import scalatags.JsDom.all._

object TypedSelect {
  def apply[A](property: Property[A], options: Seq[(A, Frag)], xs: Modifier*): html.Element = {
    val elem = select(options.map(opt => {
      val in = option(opt._2).render
      in.selected = property.get == opt._1
      property.listen { _ =>
        in.selected = property.get == opt._1
      }
      in
    }), xs).render
    elem.onchange = (_: Event) => {
      property.set(options(elem.selectedIndex)._1)
    }
    elem
  }

}

