package io.gitlab.sklavedaniel.gaia.milovanaviewer

import io.gitlab.sklavedaniel.gaia.shared.ScriptInterpreter.TeaseInfo
import io.udash.wrappers.jquery.jQ
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.Element
import org.scalajs.dom.ext.Ajax
import upickle.default._

import scala.scalajs.js.annotation.JSExportTopLevel
import scala.util.{Failure, Success, Try}

@JSExportTopLevel("gaia.Main")
object Main {

  def main(args: Array[String]): Unit = {
    Ajax.get("assets/teases/metadata.txt").onComplete {
      case Success(result) =>
        Try(read[Map[String, TeaseInfo]](result.responseText)) match {
          case Success(metadata) =>
            init(Some(metadata))
          case Failure(e) =>
            println("Could not parse tease cache metadata: " + e.getMessage)
            init(None)
        }
      case Failure(e) =>
        println("Could not load tease cache metadata: " + e.getMessage)
        init(None)
    }
  }

  def init(metadata: Option[Map[String, TeaseInfo]]): Unit = {
    val viewer = new Viewer(metadata)
    jQ((jThis: Element) => {
      val appRoot = jQ("#application").get(0)
      viewer.MyApp.run(appRoot.get)
    })
  }

}
