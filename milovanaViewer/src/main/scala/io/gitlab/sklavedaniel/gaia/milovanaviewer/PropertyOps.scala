package io.gitlab.sklavedaniel.gaia.milovanaviewer

import io.udash._

object PropertyOps {

  implicit class ReadablePropertyWrapper[A](property: ReadableProperty[A]) {
    def is(value: A): ReadableProperty[Boolean] = {
      property.transform(_ == value)
    }
  }

  implicit class BooleanPropertyWrapper(property: ReadableProperty[Boolean]) {
    def ||(other: ReadableProperty[Boolean]): ReadableProperty[Boolean] =
      property.combine(other)(_ || _)

    def &&(other: ReadableProperty[Boolean]): ReadableProperty[Boolean] =
      property.combine(other)(_ && _)

    def ===(other: ReadableProperty[Boolean]): ReadableProperty[Boolean] =
      property.combine(other)(_ == _)

    def ==>(other: ReadableProperty[Boolean]): ReadableProperty[Boolean] =
      (!property).combine(other)(_ || _)

    def unary_!(): ReadableProperty[Boolean] = property.transform(!_)


  }

}
