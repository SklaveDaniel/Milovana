package io.gitlab.sklavedaniel.gaia.milovanaviewer

import io.udash._
import org.scalajs.dom.html
import org.scalajs.dom.Event

import scalatags.JsDom
import scalatags.JsDom.all._

object TypedRadioButtons {
  def apply[A, B](property: Property[A], options: Seq[(A, B)], decorator: Seq[(html.Input, A, B)] => JsDom.TypedTag[html.Element], xs: Modifier*): JsDom.TypedTag[html.Element] = {
    val uuid = java.util.UUID.randomUUID().toString
    decorator(options.map(opt => {
      val in = input(tpe := "radio", name := uuid, onchange :+= ((event: Event) => {
        property.set(opt._1)
      }))(xs: _*).render
      in.checked = property.get == opt._1
      property.listen { _ =>
        in.checked = property.get == opt._1
      }
      (in, opt._1, opt._2)
    }))
  }

}

