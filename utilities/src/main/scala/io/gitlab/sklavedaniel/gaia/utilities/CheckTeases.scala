package io.gitlab.sklavedaniel.gaia.utilities

import java.io.{File, PrintStream}

import io.gitlab.sklavedaniel.gaia.shared.ScriptParser

import scala.io.Source
import scala.util.{Failure, Success}


object CheckTeases {

  def main(argv: Array[String]): Unit = {
    val Array(baseDirName, scriptName, successLogName, failLogName, errorLogName) = argv
    def files = recursiveListFiles(new File(baseDirName))
    def scripts = files.filter(_.getName == scriptName).map { file =>
      println("Processing: " + file.getPath)
      val bs = Source.fromFile(file, "utf8")
      val s = bs.mkString
      bs.close()
      (file, ScriptParser.parse(s))
    }
    val successLog = new PrintStream(successLogName)
    val failLog = new PrintStream(failLogName)
    val errorLog = new PrintStream(errorLogName)
    for (script <- scripts) {
      script match {
        case (file, Success(script)) =>
          successLog.println(file.getPath)
        case (file, Failure(e)) =>
          failLog.println(file.getPath)
          errorLog.println("#" * 100)
          errorLog.println(file.getPath)
          e.printStackTrace(errorLog)
          errorLog.println()
          errorLog.println()
      }
    }
    successLog.close()
    failLog.close()
    errorLog.close()
  }

  def recursiveListFiles(f: File): Stream[File] = {
    val these = f.listFiles.toStream
    these.filter(_.isFile) ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }
}
