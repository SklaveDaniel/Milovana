package io.gitlab.sklavedaniel.gaia.utilities

import java.io.{File, FileWriter, PrintStream}

import io.gitlab.sklavedaniel.gaia.shared.ScriptInterpreter.TeaseInfo
import io.gitlab.sklavedaniel.gaia.shared.{ScriptInterpreter, ScriptParser}
import upickle.default._

import scala.collection.mutable
import scala.io.Source
import scala.sys.process.Process
import scala.util.{Failure, Success, Try}

object GenerateTeaseDB {

  def main(argv: Array[String]): Unit = {
    val Array(baseDirName, dbpath) = argv

    val alllog = new PrintStream("all.log")
    val issuelog = new PrintStream("issues.log")

    def files = recursiveListFiles(new File(baseDirName))

    val idrgx = """(\d+)/script""".r

    def scripts = files.filter(_.getName == "script").flatMap { file =>
      println("Processing: " + file.getPath)
      val original = new File(file.getParentFile, "script.original")
      val id = idrgx.findFirstMatchIn(file.getPath).get.group(1)
      alllog.println(file.getPath)
      Try(Source.fromFile(original, "utf8").mkString) match {
        case Failure(_) =>
          issuelog.println(s"no original ${file.getPath}")
          None
        case Success(source) =>
          ScriptParser.parse(source) match {
            case Success(_) =>
              None
            case Failure(_) =>
              ScriptParser.parse(Source.fromFile(file, "utf8").mkString) match {
                case Failure(_) =>
                  issuelog.println(s"Failed parser ${file.getPath}")
                  None
                case Success(result) =>
                  Try(ScriptInterpreter.toScript(result)) match {
                    case Failure(e) =>
                      issuelog.println(s"Failed interpreter ${file.getPath}")
                      None
                    case Success(script) =>
                      val metadata = Try {
                        val bs2 = Source.fromFile(new File(file.getParentFile, "metadata"))
                        val List(title, authorid, authorname, "") = bs2.getLines().toList
                        bs2.close()
                        TeaseInfo(title, authorname, authorid, "milovana.com")
                      }
                      if (metadata.isFailure) {
                        issuelog.println(s"Failed metadata ${file.getPath}")
                      }
                      metadata.toOption.map(m => (id, script, m))
                  }
              }
          }
      }
    }

    val success = mutable.HashMap[String, TeaseInfo]()
    val duplicates = mutable.HashSet[String]()

    for ((id, script, metadata) <- scripts) {
      success += id -> metadata
      val out = new FileWriter(dbpath + "/" + id + ".txt")
      out.write(write(script))
      out.close()
      Process("gzip" :: "-fn" :: (id + ".txt") :: Nil, Some(new File(dbpath))).run()

    }

    val out = new FileWriter(dbpath + "/metadata.txt")
    out.write(write(success.toMap))
    out.close()

    println()
    println("### Duplicates")
    println(duplicates.mkString("\n"))

    Process("gzip" :: "-fn" :: "metadata.txt" :: Nil, Some(new File(dbpath))).run()
  }

  def recursiveListFiles(f: File): Stream[File] = {
    val these = f.listFiles.toStream
    these.filter(_.isFile) ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }

}
