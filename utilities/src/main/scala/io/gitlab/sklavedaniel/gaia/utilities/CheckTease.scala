package io.gitlab.sklavedaniel.gaia.utilities

import io.gitlab.sklavedaniel.gaia.shared.{ScriptInterpreter, ScriptParser}

import scala.io.Source
import scala.util.{Failure, Success}

object CheckTease {

  def main(argv: Array[String]): Unit = {
    val Array(scriptName) = argv
    val script = ScriptParser.parse(Source.fromFile(scriptName).mkString).map(ScriptInterpreter.toScript)
    script match {
      case Failure(e) => e.printStackTrace()
      case Success(_) => println("Success!")
    }
  }
}
