package io.gitlab.sklavedaniel.gaia.utilities

import java.io.{File, PrintStream}

import io.gitlab.sklavedaniel.gaia.shared.ScriptParser

import scala.io.Source
import scala.util.{Failure, Success}

object CheckSelectedTeases {

  def main(argv: Array[String]): Unit = {
    val Array(fileListName, successLogName, failLogName, errorLogName) = argv
    def scripts = Source.fromFile(fileListName).getLines().toStream.map { f =>
      println("Processing: " + f)
      val file = new File(f)
      (file, ScriptParser.parse(Source.fromFile(file, "utf8").mkString))
    }
    val successLog = new PrintStream(successLogName)
    val failLog = new PrintStream(failLogName)
    val errorLog = new PrintStream(errorLogName)
    for (script <- scripts) {
      script match {
        case (file, Success(script)) =>
          successLog.println(file.getPath)
        case (file, Failure(e)) =>
          failLog.println(file.getPath)
          errorLog.println("#" * 100)
          errorLog.println(file.getPath)
          e.printStackTrace(errorLog)
          errorLog.println()
          errorLog.println()
      }
    }
    successLog.close()
    failLog.close()
    errorLog.close()
  }

  def recursiveListFiles(f: File): Stream[File] = {
    val these = f.listFiles.toStream
    these.filter(_.isFile) ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }
}

